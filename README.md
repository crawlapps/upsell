# Shopify APP - Upsell

### Installation
Install the dependencies and devDependencies.

For both environment
```sh
$ composer install
$ cp .env.example .env 
$ nano .env // set all credentials(ex: database, shopify api key and secret, mail credentials)
$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed
```

 (1) Update Database Details in .env file
 
 
     DB_CONNECTION=mysql
     DB_HOST=127.0.0.1
     DB_PORT=3306
     DB_DATABASE=laravel
     DB_USERNAME=root
     DB_PASSWORD=



  (2) Edit Shopify APP Credential Here in .env file
  
    SHOPIFY_API_KEY=
    SHOPIFY_API_SECRET=


 (3) For development environments...

```sh
$ npm install
$ npm run dev
```
   For production environments...

```sh
$ npm install --production
$ npm run prod
```

   (4) Use This Command for clear cache App
   
     $  php artisan optimize:clear     
     $  php artisan config:clear
     $  php artisan cache:clear
     

 (5) Setup superviser for Queue JOB in Server

    $ sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl restart [superviser-name]


   
### Used Shopify Tools

* Admin rest-api, graphQL api
* App-bridge


