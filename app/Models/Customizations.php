<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customizations extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'customizations';

    protected $fillable = ['user_id','is_app_enable','settings'];

    protected $casts = [
        'settings' => 'array',
    ];

    protected $dates = ['deleted_at'];

    //**settings Fields ================================== */
    //
    // upsell_add_btn_bg_color
    // upsell_add_btn_bg_hover_color
    // upsell_add_btn_text_font_style
    // upsell_add_btn_text_font_size
    // upsell_add_btn_text_font_weight
    // upsell_add_btn_text_font_color
    // upsell_add_btn_size
    // upsell_product_name_text_size
    // upsell_product_name_text_font_style
    // upsell_product_name_text_font_weight
    // upsell_product_name_text_font_color
    // add_to_cart_popup_is_turn_on
    // upsell_is_position



}
