<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'products';

    protected $fillable = ['offer_id','shopify_id','gid','title','handle','image'];

    protected $dates = ['deleted_at'];
    
    public function upSells(){
             return $this->hasMany(upSellsProducts::class, 'product_db_id', 'id');
    }     

}
