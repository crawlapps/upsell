<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class LineItem extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'line_items';

    protected $fillable = ['db_order_id','shopify_lineitem_id','name','price','product_id','quantity','variant_id','full_response'];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'full_response' => 'array'       
    ];

    protected $newDateFormat = 'd-m-Y H:i:s';

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format($this->newDateFormat);
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format($this->newDateFormat);
    }

}
