<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'customers';

    protected $fillable = [
        'user_id',
        'shopify_customer_id',
        'email',
        'accepts_marketing',
        'verified_email',
        'currency',
        'admin_graphql_api_id',
        'default_address',
        'upsell_orders_count'
     ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'default_address' => 'array'       
    ];

    protected $newDateFormat = 'd-m-Y H:i:s';

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format($this->newDateFormat);
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format($this->newDateFormat);
    }
 
}
