<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class upSellsOffer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'up_sells_offers';

    protected $fillable = ['user_id','name','title','status'];

    protected $dates = ['deleted_at'];

    public function products(){
             return $this->hasMany(Products::class, 'offer_id', 'id');
    }  

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d-m-Y');
    }
    
    public function Analytics(){
        return $this->hasMany(upSellsOfferAnalytics::class, 'offer_id', 'id');
    }

}
