<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class upSellsOfferAnalytics extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'up_sells_offer_analytics';
 
    protected $fillable = ['user_id','offer_id','click_count'];

    protected $dates = ['deleted_at'];
  
}
