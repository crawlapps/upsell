<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class upSellsProducts extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'up_sells_products';
 
    protected $fillable = ['product_db_id','product_id_1','shopify_id','handle','user_id','title','image'];

    protected $dates = ['deleted_at'];

    public function upSellsVariants(){
        return $this->hasMany(upSellsProductVariants::class, 'product_id_1', 'id');
    }

}
