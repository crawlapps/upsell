<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class upSellsProductVariants extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'up_sells_product_variants';

    protected $fillable = ['product_id_1','product_id_2','shopify_id','title','image','price','selectedOptions'];

    protected $casts = [
        'selectedOptions' => 'array'       
    ];

    protected $dates = ['deleted_at'];
}
