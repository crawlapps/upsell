<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'orders';

    protected $fillable = ['user_id','shopify_order_id','name','fulfillment_status','full_response'];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'full_response' => 'array'       
    ];

    protected $newDateFormat = 'd-m-Y H:i:s';

    public function getUpdatedAtAttribute($value) {
        return Carbon::parse($value)->format($this->newDateFormat);
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format($this->newDateFormat);
    }

    public function LineItems(){
        return $this->hasMany(LineItem::class, 'db_order_id', 'id' );
    }


}
