<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ShopifyTrait;
use Exception;
use Auth;
use Response;
use App\Jobs\OrdersCreateJob;
use App\Jobs\OrdersUpdatedJob;
use App\Jobs\ProductsUpdateJob;

use App\Models\Products;
use App\Models\upSellsProducts;
use App\Models\upSellsProductVariants;
use App\Models\upSellsOffer;

class TestController extends Controller
{

    use ShopifyTrait;

    public function __construct(){

    }

    public function index(){
         return ["data"=>"resposne"];
    }

    public function shopData(){

        try{
            $data = $this->shops();

            dd($data);
        }catch(Exception $e){
            return $e->getMessage();
        }
        
    }

    public function getShopifyProducts(){
             try{

                $shop = Auth::user();

                $productsInfo = [];
            
                $shopInfo = $this->shopify_getShopInfo($shop);
                $result = $this->shopify_GetProducts($shop);

                if($result['products']){
                 $productsInfo = $this->createResponseProductsFromQl($result['products']);
                }

                return Response::json([
                     'success' => true,
                     'domain' => $shop->name,
                     'shopInfo'  => $shopInfo,
                     'products' => $productsInfo,
                     'pageInfo' =>  $result['pageInfo']                   
                ]);
                    

             }catch(Exception $e){
                 return $e->getMessage();
             }
    }

    public function orderjob(Request $request){
        
        $shop = Auth::user(); 
        $shopDomain = $shop->name;
        $data = [];

        $order  = '{"id":4446416109754,"admin_graphql_api_id":"gid://shopify/Order/4446416109754","app_id":580111,"browser_ip":"49.36.85.175","buyer_accepts_marketing":true,"cancel_reason":null,"cancelled_at":null,"cart_token":null,"checkout_id":27000223826106,"checkout_token":"d8936128db739d0827f04f04fb29ac6e","client_details":{"accept_language":"en-GB,en-US;q=0.9,en;q=0.8","browser_height":935,"browser_ip":"49.36.85.175","browser_width":1905,"session_hash":null,"user_agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36"},"closed_at":null,"confirmed":true,"contact_email":"satish.crawlapps@gmail.com","created_at":"2022-04-13T12:46:05+05:30","currency":"INR","current_subtotal_price":"1196.34","current_subtotal_price_set":{"shop_money":{"amount":"1196.34","currency_code":"INR"},"presentment_money":{"amount":"1196.34","currency_code":"INR"}},"current_total_discounts":"0.00","current_total_discounts_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"current_total_duties_set":null,"current_total_price":"1411.68","current_total_price_set":{"shop_money":{"amount":"1411.68","currency_code":"INR"},"presentment_money":{"amount":"1411.68","currency_code":"INR"}},"current_total_tax":"215.34","current_total_tax_set":{"shop_money":{"amount":"215.34","currency_code":"INR"},"presentment_money":{"amount":"215.34","currency_code":"INR"}},"customer_locale":"en","device_id":null,"discount_codes":[],"email":"satish.crawlapps@gmail.com","estimated_taxes":false,"financial_status":"paid","fulfillment_status":null,"gateway":"bogus","landing_site":"/wallets/checkouts.json","landing_site_ref":null,"location_id":null,"name":"#1074","note":null,"note_attributes":[],"number":74,"order_number":1074,"order_status_url":"https://crawlapps-satish.myshopify.com/58240893114/orders/f0acd6cbaba61eb168c7683b9db009ad/authenticate?key=ff9bb9f909967798215aca332a40dbaf","original_total_duties_set":null,"payment_gateway_names":["bogus"],"phone":null,"presentment_currency":"INR","processed_at":"2022-04-13T12:46:05+05:30","processing_method":"direct","reference":null,"referring_site":"https://crawlapps-satish.myshopify.com/products/xiaomi-mi-true-wireless-earbuds-basic-2-airdots-2-auriculares-bluetooth-auriculares-inalambricos-bluetooth-5-0-estuche-de-carga-magneticaversion-global-negro-4","source_identifier":null,"source_name":"web","source_url":null,"subtotal_price":"1196.34","subtotal_price_set":{"shop_money":{"amount":"1196.34","currency_code":"INR"},"presentment_money":{"amount":"1196.34","currency_code":"INR"}},"tags":"","tax_lines":[{"price":"215.34","rate":0.18,"title":"IGST","price_set":{"shop_money":{"amount":"215.34","currency_code":"INR"},"presentment_money":{"amount":"215.34","currency_code":"INR"}},"channel_liable":false}],"taxes_included":false,"test":true,"token":"f0acd6cbaba61eb168c7683b9db009ad","total_discounts":"0.00","total_discounts_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"total_line_items_price":"1196.34","total_line_items_price_set":{"shop_money":{"amount":"1196.34","currency_code":"INR"},"presentment_money":{"amount":"1196.34","currency_code":"INR"}},"total_outstanding":"0.00","total_price":"1411.68","total_price_set":{"shop_money":{"amount":"1411.68","currency_code":"INR"},"presentment_money":{"amount":"1411.68","currency_code":"INR"}},"total_price_usd":"18.61","total_shipping_price_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"total_tax":"215.34","total_tax_set":{"shop_money":{"amount":"215.34","currency_code":"INR"},"presentment_money":{"amount":"215.34","currency_code":"INR"}},"total_tip_received":"0.00","total_weight":0,"updated_at":"2022-04-13T12:46:06+05:30","user_id":null,"billing_address":{"first_name":"Satish-1","address1":"33 -1st floor A-wing, Gokuldham, Gokuldham, Gokuldham","phone":null,"city":"surat","zip":"395006","province":"Gujarat","country":"India","last_name":"parmar","address2":"Gokuldham","company":null,"latitude":21.2233207,"longitude":72.8527249,"name":"Satish-1 parmar","country_code":"IN","province_code":"GJ"},"customer":{"id":5621906112698,"email":"satish.crawlapps@gmail.com","accepts_marketing":true,"created_at":"2021-10-19T11:22:50+05:30","updated_at":"2022-04-13T12:46:06+05:30","first_name":"Satish-1","last_name":"parmar","orders_count":0,"state":"disabled","total_spent":"0.00","last_order_id":null,"note":null,"verified_email":true,"multipass_identifier":null,"tax_exempt":false,"phone":null,"tags":"","last_order_name":null,"currency":"INR","accepts_marketing_updated_at":"2021-12-08T14:11:46+05:30","marketing_opt_in_level":"single_opt_in","admin_graphql_api_id":"gid://shopify/Customer/5621906112698","default_address":{"id":7084879511738,"customer_id":5621906112698,"first_name":"Satish-1","last_name":"parmar","company":null,"address1":"33 -1st floor A-wing, Gokuldham, Gokuldham, Gokuldham","address2":"Gokuldham","city":"surat","province":"Gujarat","country":"India","zip":"395006","phone":null,"name":"Satish-1 parmar","province_code":"GJ","country_code":"IN","country_name":"India","default":true}},"discount_applications":[],"fulfillments":[],"line_items":[{"id":11203660185786,"admin_graphql_api_id":"gid://shopify/LineItem/11203660185786","fulfillable_quantity":1,"fulfillment_service":"manual","fulfillment_status":null,"gift_card":false,"grams":0,"name":"Xiaomi Mi True Wireless Earbuds Basic 2,Airdots 2 Auriculares Bluetooth, Auriculares Inalámbricos Bluetooth 5.0,Estuche de Carga Magnética(Versión Global) Negro","origin_location":{"id":2992331653306,"country_code":"IN","province_code":"GJ","name":"crawlapps satish","address1":"4022 Silver Business Point, VIP Cir","address2":"","city":"SURAT","zip":"394105"},"price":"1196.34","price_set":{"shop_money":{"amount":"1196.34","currency_code":"INR"},"presentment_money":{"amount":"1196.34","currency_code":"INR"}},"product_exists":true,"product_id":6989112279226,"properties":[],"quantity":1,"requires_shipping":true,"sku":"","taxable":true,"title":"Xiaomi Mi True Wireless Earbuds Basic 2,Airdots 2 Auriculares Bluetooth, Auriculares Inalámbricos Bluetooth 5.0,Estuche de Carga Magnética(Versión Global) Negro","total_discount":"0.00","total_discount_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"variant_id":40962811068602,"variant_inventory_management":null,"variant_title":"","vendor":"crawlapps satish","tax_lines":[{"channel_liable":false,"price":"215.34","price_set":{"shop_money":{"amount":"215.34","currency_code":"INR"},"presentment_money":{"amount":"215.34","currency_code":"INR"}},"rate":0.18,"title":"IGST"}],"duties":[],"discount_allocations":[]}],"payment_details":{"credit_card_bin":"1","avs_result_code":null,"cvv_result_code":null,"credit_card_number":"•••• •••• •••• 1","credit_card_company":"Bogus"},"refunds":[],"shipping_address":{"first_name":"Satish-1","address1":"33 -1st floor A-wing, Gokuldham, Gokuldham, Gokuldham","phone":null,"city":"surat","zip":"395006","province":"Gujarat","country":"India","last_name":"parmar","address2":"Gokuldham","company":null,"latitude":21.2233207,"longitude":72.8527249,"name":"Satish-1 parmar","country_code":"IN","province_code":"GJ"},"shipping_lines":[{"id":3673054904506,"carrier_identifier":null,"code":"Standard","delivery_category":null,"discounted_price":"0.00","discounted_price_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"phone":null,"price":"0.00","price_set":{"shop_money":{"amount":"0.00","currency_code":"INR"},"presentment_money":{"amount":"0.00","currency_code":"INR"}},"requested_fulfillment_service_id":null,"source":"shopify","title":"Standard","tax_lines":[],"discount_allocations":[]}]}'; 
      //  dd(json_decode($order));
        $data = json_decode($order);

        OrdersCreateJob::dispatch($shopDomain,$data);
       // OrdersUpdatedJob::dispatch($shopDomain,$data);

    }

   public function productUpdatejob(){
            $shop = Auth::user(); 
            $shopDomain = $shop->name;

            $sh_productID = 4446416109754; 
            
            $user_id = $shop->id; 

            $upsells_product =  upSellsProducts::where('shopify_id', $sh_productID)->where('user_id', $user_id)->get();  
            if(count($upsells_product) > 0){

                foreach($upsells_product as $products){

                    $product_id_1 = $upsells_product['id'];

                    $variants  = upSellsProductVariants::where('product_id_1', $product_id_1)->where('user_id', $user_id)->get();  

                    if(count($variants) > 0){
                        foreach ( $variants as $variant ){

                             $product_id_2 = $variant['product_id_2'];
                             $query_params_variant = ["id" => $product_id_2];                                
                             $variant_res = $this->shopify_GetVariantByID($shop,$query_params_variant);
                             if($variant_res){ 
                             $variant_price = $variant_res['price'];                               
                             $variant_image = $variant_res['image'];
                             $variant_title = $variant_res['title']; 

                             $db_variant = upSellsProductVariants::where('id', $variant['id'])                                             
                                             ->update([
                                                       'price' => $variant_price,
                                                       'image' => $variant_image,
                                                       'title' => $variant_title
                                                      ]);
                             }

                        }  
                    }  
                }

                $db_upsells_product =  upSellsProducts::where('shopify_id', $sh_productID)->where('user_id', $user_id)
                                        ->update([                                               
                                                'image' => $data->image['src'],
                                                'title' => $data->title
                                                ]);


            }          
      
   }



}
