<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ShopifyTrait;
use App\Traits\SettingsTrait;
use Exception;
use Response;
use Auth;

class InstallationController extends Controller
{
    use ShopifyTrait;
    use SettingsTrait;
    

     //getShopThemes==============================================================================================================
          public function getShopThemes(Request $request){
            try{
                   $shop = Auth::user();    
                   $shopThemesInfo = $this->shopify_getShopThemes($shop);
                  
                   return Response::json([
                          'success' => true,                           
                          'themes'  => $shopThemesInfo,                         
                          ],200); 

            }catch(Exception $e){
              return Response::json([
                  "success" => false,
                  "message" => $e->getMessage()             
              ]);
            }
     }
     //@end ::getShopThemes=========================================================================================================

     public function installShopThemes(Request $request){
        try{
              
            $input = $request->all();
            $theme_id = $input['theme_id'];
            $shop = Auth::user(); 
            
            logger("===================================================================");
            logger("========================================================================");
            logger("theme_id => ".$theme_id);
            logger("=========================================================================="); 

                        
            $asset_files = [
              'sections/main-product.liquid',
              'sections/product.liquid', 
              'snippets/product-form.liquid', 
              'templates/product.liquid',  
              'sections/product-template.liquid',
              'snippets/product-page-design-default.liquid',
              'snippets/pr_summary.liquid'                        
            ];

            foreach($asset_files as $key => $asset_file_key){

              logger("key loop => ". $key);
              logger("asset[key] => ".$asset_file_key);
              
              $asset = $shop->api()->rest('GET', '/admin/themes/'.$theme_id.'/assets.json',
              ["asset[key]" => $asset_file_key]);

              logger("asset find file res is error => ".$asset['errors']);
                            
              if(!$asset['errors']){
                $assetHtml = $asset['body']['asset']['value']; 
             
                $install_status = $this->modifyThemeLiquid($shop,$theme_id,$asset_file_key,$assetHtml);    
                logger("install_status =>". $install_status);
                if($install_status){
                     return  Response::json(['success' => true, 'message' => 'Snippets installation has been successfully.'], 200); 
                    break;  
                    
                }          
              } 

            }              

            return  Response::json(['success' => false, 'message' => 'We could not able to find the default file or product form. Please try to install it manually.'], 200);          

        }catch(Exception $e){
          return Response::json([
              "success" => false,
              "message" => $e->getMessage()             
          ]);
        }
 }

    public function modifyThemeLiquid($shop,$theme_id,$asset_key,$assetHtml){

         try{  

        logger("=========================START :: modifyThemeLiquid===============================");
       
       
          
        $defaultSettings = $this->getDefaultSettings($shop->id);
              
        $upsell_grid_placement = '';

            
      if($defaultSettings) {
          $upsell_grid_placement = $defaultSettings['upsell_grid_placement'];
      }

      $search_string = '<product-form class="product-form">';
      $new_string = '<div class="upsell-offers"></div>';
    
      if($assetHtml){

        $existHtml = $assetHtml;
           
         // remove if exist
         if (strpos($existHtml,$new_string)) {
         
                $existHtml = str_replace($new_string,'', $existHtml);
         }     

        $repalceString = "";

        if($asset_key == 'snippets/product-form.liquid' || $asset_key == 'sections/product-template.liquid' ){

            logger("Exist asset-key => ".$asset_key);

            if($upsell_grid_placement=="above_add_to_cart_button"){
                $search_string = "{%- form 'product'";
                if(strpos($existHtml,$search_string) !== false) {
                    $repalceString = $new_string.'<br>'.$search_string;
                }
            }

            if ($upsell_grid_placement == "below_add_to_cart_button") {
                         
                $search_string = '{% endform %}';
                if(strpos($existHtml,$search_string) !== false) {
                  $repalceString = $search_string.'<br>'.$new_string;
                                  
                }else{                 

                  $search_string = '{%- endform -%}';
                  if(strpos($existHtml,$search_string) !== false) {
                    $repalceString = $search_string.'<br>'.$new_string;
                                    
                  }

                }
             
            } 

            if(empty($repalceString)){

             $search_string = '</form>';
                if(strpos($existHtml,$search_string) !== false) {
                  $repalceString = $search_string.'<br>'.$new_string;
                                  
                }
           }

        }else{

          logger("Exist asset-key => ".$asset_key);

            if($upsell_grid_placement=="above_add_to_cart_button"){
                $search_string = '<product-form class="product-form">';
                if(strpos($existHtml,$search_string) !== false) {
                    $repalceString = $new_string.'<br>'.$search_string;
                                   
                }else{

                  $search_string = '<form action="/cart/add"';
                  if(strpos($existHtml,$search_string) !== false) {
                    $repalceString = $new_string.'<br>'.$search_string;
                                    
                  }
                }
            }

            if ($upsell_grid_placement == "below_add_to_cart_button") {
              $search_string = "</product-form>";
              if(strpos($existHtml,$search_string) !== false) {
                  $repalceString =  $search_string.'<br>'.$new_string;            
              }else{
                $search_string = '</form>';
                if(strpos($existHtml,$search_string) !== false) {
                  $repalceString = $search_string.'<br>'.$new_string;
                                  
                }
              }
            }          
         } 


         if(empty($repalceString)){

          $search_string = '{% endform %}';
          if(strpos($existHtml,$search_string) !== false) {
            $repalceString = $search_string.'<br>'.$new_string;
                            
          }else{                 

            $search_string = '{%- endform -%}';
            if(strpos($existHtml,$search_string) !== false) {
              $repalceString = $search_string.'<br>'.$new_string;
                              
            }
          }
       }

         
         logger("====repalceString=====");
         logger($repalceString);

        if(!empty($repalceString)){
         $asset_value =  str_replace($search_string,$repalceString,$existHtml);
                      
          $updatestatus = $this->shopify_updateThemeLiquid($shop,$theme_id,$asset_key,$asset_value);
          if($updatestatus){
            return true;
          }

        }                

     }  

     return false;
     logger("=========================END :: modifyThemeLiquid===============================");
        }catch(Exception $e){
          logger("=========================ERROR :: modifyThemeLiquid===============================");
          return Response::json([
              "success" => false,
              "message" => $e->getMessage()             
          ]);
        }
    }
     


    public function unInstallShopThemesSnippets(Request $request){
      try{
            
        logger("=========================START :: unInstallShopThemes ===============================");
          $input = $request->all();
          $theme_id = $input['theme_id'];
          $shop = Auth::user(); 
          
          logger("===================================================================");
          logger("========================================================================");
          logger("theme_id => ".$theme_id);
          logger("=========================================================================="); 

                      
          $asset_files = [
            'sections/main-product.liquid',
            'sections/product.liquid', 
            'snippets/product-form.liquid', 
            'templates/product.liquid',  
            'sections/product-template.liquid',
            'snippets/product-page-design-default.liquid',
            'snippets/pr_summary.liquid'                        
          ];

          foreach($asset_files as $key => $asset_file_key){

            logger("key loop => ". $key);
            logger("asset[key] => ".$asset_file_key);
            
            $asset = $shop->api()->rest('GET', '/admin/themes/'.$theme_id.'/assets.json',
            ["asset[key]" => $asset_file_key]);

            logger("asset find file res is error => ".$asset['errors']);
                          
            if(!$asset['errors']){
              $assetHtml = $asset['body']['asset']['value']; 
           
              $uninstall_status = $this->modifyThemeLiquidForUninstall($shop,$theme_id,$asset_file_key,$assetHtml);    
              logger("Uninstall_status =>". $uninstall_status);
              if($uninstall_status){
                   return  Response::json(['success' => true, 'message' => 'Snippets Uninstallation has been successfully.'], 200); 
                   break;  
                  
              }          
            } 

          }              

          return  Response::json(['success' => false, 'message' => 'We could not able to uninstall snippets or uninstalled already . Please try to Uninstall it manually.'], 200);          

      }catch(Exception $e){
        return Response::json([
            "success" => false,
            "message" => $e->getMessage()             
        ]);
      }
}

    public function modifyThemeLiquidForUninstall($shop,$theme_id,$asset_key,$assetHtml){

      try{  

              logger("=========================START :: modifyThemeLiquid ForUninstall ===============================");
             
              $search_string = '<div class="upsell-offers"></div>';

              if($assetHtml){

               $existHtml = $assetHtml;
                  
                // remove if exist
                if (strpos($existHtml,$search_string)) {
                
                      $asset_value = str_replace($search_string,'', $existHtml);    
                            
                      $updatestatus = $this->shopify_updateThemeLiquid($shop,$theme_id,$asset_key,$asset_value);
                      if($updatestatus){
                        return true;
                      }
                   }   
              }  

              return false;
              logger("=========================END :: modifyThemeLiquid ForUninstall===============================");
              }catch(Exception $e){
                logger("=========================ERROR :: modifyThemeLiquid ForUninstall===============================");
                return Response::json([
                    "success" => false,
                    "message" => $e->getMessage()             
                ]);
              }
    }



}
