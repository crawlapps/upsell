<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customizations;
use Response;
use Exception;
use Auth;
use App\Traits\SettingsTrait;

class CustomizationsController extends Controller
{

    use SettingsTrait;
    
    
public function getSettings(Request $request){

    try{
            
        $shop = Auth::user(); 

        $settings = Customizations::where('user_id',$shop->id)->value('settings');

        return Response::json([
            'success' => true,
            'domain' => $shop->name,            
            'settings' => $settings  
       ],200); 
        
    }catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
        ],422);                
    }

}   
 public function saveSettings(Request $request){

     try{
            
        $shop = Auth::user(); 

        $input = $request->all();

        $form = $input['form'];
     //   $settings = json_encode($form);
        
       $customizations = Customizations::firstOrNew(['user_id' => $shop->id]);
       $customizations->settings = $form;
       $customizations->save();

        return  Response::json(['success' => true, 'message' => 'Settings save successfully.'], 200);    
        
     }catch(Exception $e){
          return Response::json([
               "success" => false,
               "data" => [],
               "message" => $e->getMessage()
        ],422);                
     }

 }
 
 public function getStatusForApp(Request $request){

    try{
            
        $shop = Auth::user(); 

        $is_app_enable = Customizations::where('user_id',$shop->id)->value('is_app_enable');

        return Response::json([
            'success' => true,
            'shop' => $shop,            
            'is_app_enable' => $is_app_enable  
       ],200); 
        
    }catch(Exception $e){
            return Response::json([
                "success" => false,               
                "message" => $e->getMessage()
        ],422);                
    }

}  

 public function changeStatusForApp(Request $request){

       //@purpos : Enable app (toggle button) the merchant will be able to disable the app from his/her store.

        try{
                    
           $shop = Auth::user(); 

           $input = $request->all();

           $is_app_enable_status = $input['is_app_enable_status'];     
            
          $AppStatusUpdate = Customizations::where('user_id',$shop->id)->update([
            "is_app_enable" => $is_app_enable_status
          ]);  

          $message = "App status change successfully.";
         
          if($is_app_enable_status){
           $message = "Enabled the app from your store successfully.";
          }else{
            $message = "Disabled the app from your store successfully.";
          }

          return  Response::json(['success' => true, 'message' => $message], 200);    
            
        }catch(Exception $e){
            return Response::json([
                "success" => false,             
                "message" => $e->getMessage()
            ],422);                
        }


 }

 
 public function resetSettings(Request $request){

    try{
           
       $shop = Auth::user(); 

       $this->SaveDefaultSettings($shop->id);

       return  Response::json(['success' => true, 'message' => 'Settings reset successfully.'], 200);    
       
    }catch(Exception $e){
         return Response::json([
              "success" => false,
              "data" => [],
              "message" => $e->getMessage()
       ],422);                
    }

}

}
