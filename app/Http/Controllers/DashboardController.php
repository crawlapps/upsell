<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\AnalyticsTrait;
use App\Traits\ShopifyTrait;
use Exception;
use Response;
use Auth;
use Carbon\Carbon;
use Symfony\Component\Intl\Currencies;
class DashboardController extends Controller
{
       use AnalyticsTrait;
       use ShopifyTrait;

       public function __construct(){

       }
      

        //shop==============================================================================================================
        public function getShopInfo(Request $request){
              try{
                     $shop = Auth::user();    
                     $shopInfo = $this->shopify_getShopInfo($shop);
                     $currency_symbol = Currencies::getSymbol($shopInfo->currency);   
                     return Response::json([
                            'success' => true,                           
                            'shopInfo'  => $shopInfo,
                            "currency_symbol" => $currency_symbol                          
                            ],200); 

              }catch(Exception $e){
                return Response::json([
                    "success" => false,
                    "message" => $e->getMessage()             
                ]);
              }
       }
       //@end ::shop=========================================================================================================


       //Total Records Analytics==============================================================================================================
       public function getTotalAnalyticsInfo(Request $request){
              try{


                     $input = $request->all();
                     $shop = Auth::user();    
                     $shopInfo = $this->shopify_getShopInfo($shop);
                     
                     $response_data = [];

                     $filter_data = [];

                    // $date = "11/05/2022, 12:05:13";

                    // $new_date =  Carbon::parse($date)->format('Y-m-d H:i:s');
 
                    // dd($new_date);

                    if(isset($input['start_date']) && isset($input['end_date'])){

                        $start_date = $input['start_date'];
                        $end_date = $input['end_date'];

                        $filter_data['start_date'] = Carbon::parse($start_date)->format('Y-m-d H:i:s');
                        $filter_data['end_date'] = Carbon::parse($end_date)->format('Y-m-d H:i:s');
                    }                        

                     $total_order =  $this->analytics_getTotalOrderInfo($shop,$filter_data);
                     $response_data['total_order'] = $total_order;

                     $total_revenue =  $this->analytics_getTotalRevenueInfo($shop,$filter_data);
                     $response_data['total_revenue'] = $total_revenue;

                     $total_upsell_product =  $this->analytics_getTotalUpsellProductInfo($shop,$filter_data);
                     $response_data['total_upsell_product'] = $total_upsell_product;

                     $total_customer =  $this->analytics_getTotalCustomerInfo($shop,$filter_data);
                     $response_data['total_customer'] = $total_customer;

                     $total_offer_clicked =  $this->analytics_getTotalOfferClickedInfo($shop,$filter_data);
                     $response_data['total_offer_clicked'] = $total_offer_clicked;

                     $currency_symbol = Currencies::getSymbol($shopInfo->currency); 

                     return Response::json([
                                   'success' => true,
                                   'domain' => $shop->name,
                                   'shopInfo'  => $shopInfo, 
                                   "currency_symbol" => $currency_symbol,
                                   'data' =>  $response_data
                            ],200); 

              }catch(Exception $e){
                return Response::json([
                    "success" => false,
                    "message" => $e->getMessage()             
                ]);
              }
       }
       //@end :: Total Records Analytics=========================================================================================================



       //Revenue Graph Analytics==============================================================================================================
       // public function getRevenueGraphInfo(Request $request){
       //         try{

       //               $input = $request->all();
       //               $shop = Auth::user();    
       //               $shopInfo = $this->shopify_getShopInfo($shop);

       //               $response_data = [];                     
       //               $filter_data = [];

       //               if(isset($input['start_date']) && isset($input['end_date'])){
       //                  $start_date = $input['start_date'];
       //                  $end_date = $input['end_date'];

       //                  $filter_data['start_date'] = Carbon::parse($start_date)->format('Y-m-d H:i:s');
       //                  $filter_data['end_date'] = Carbon::parse($end_date)->format('Y-m-d H:i:s');
       //               }  

       //               $this->analytics_RevenueGraphInfo($shop,$filter_data);
       //               $currency_symbol = Currencies::getSymbol($shopInfo->currency);
       //               return Response::json([
       //                             'success' => true,
       //                             'domain' => $shop->name,
       //                             'shopInfo'  => $shopInfo, 
       //                             "currency_symbol" => $currency_symbol,
       //                             'data' =>  $response_data
       //                      ],200); 

       //         }catch(Exception $e){
       //               return Response::json([
       //                             "success" => false,
       //                             "message" => $e->getMessage()             
       //                  ]);
       //         }
       // }
       //@end :: Revenue Graph Analytics=========================================================================================================

}
