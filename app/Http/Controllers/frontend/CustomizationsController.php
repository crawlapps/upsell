<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Response;
use Auth;
use App\Models\User;
use App\Models\Customizations;
class CustomizationsController extends Controller
{
        
    public function getSettings(Request $request){

        try{

            $settingsInfo = [];
            $input = $request->all();
           
            $user_id = $input['user_id'];
                
            $shop = User::where('id',$user_id)->first();
               if($shop){    
                 $settings = Customizations::where('user_id',$shop->id)->value('settings');  
                    if($settings){
                        $settingsInfo = $settings;
                    }         
                }

        return Response::json([
            'success' => true,        
            'settings' => $settingsInfo  
       ],200); 
            
        }catch(Exception $e){
                return Response::json([
                    "success" => false,
                    "data" => [],
                    "message" => $e->getMessage()
            ],422);                
        }
    
    } 

}
