<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Response;
use App\Models\Products;
use App\Models\ShopifyShop;
use App\Models\upSellsProducts;
use App\Models\upSellsProductVariants;
use App\Traits\ShopifyTrait;
use Auth;
use App\Models\User;
use App\Models\Customizations;
use App\Models\upSellsOffer;
use DB;
use App\Models\upSellsOfferAnalytics;

class ManageUpsellProductsController extends Controller
{

     use ShopifyTrait; 


     public function getUpSellsSetting(Request $request){
        
      try{
         
          $settingsInfo = [];  

          $input = $request->all();
          
          $shopify_domain = $input['shopify_domain'];

          $shopifyShop = ShopifyShop::select(\DB::raw('shopify_shops.user_id'))
                ->join('users', 'shopify_shops.user_id', '=' ,'users.id')
                ->where('shopify_shops.domain', $shopify_domain)
                ->orWhere('users.name', $shopify_domain)
                ->first();

          if($shopifyShop){
            $settings = Customizations::where('user_id',$shopifyShop->user_id)->select('is_app_enable','settings')->first();  
            if($settings){
                $settingsInfo['is_app_enable'] = $settings->is_app_enable;  
                $settingsInfo['upsell_grid_placement'] = $settings->settings['upsell_grid_placement']; 
            }  
          }
        
          return response()->json([
            'success' => true, 
            'settings' => $settingsInfo,                                
        ],200);  

      }catch(Exception $e){
          return Response::json([
               "success" => false,
               "data" => [],
               "message" => $e->getMessage()
        ],422);
      }        
}

     public function getUpSellsProducts(Request $request){
        
      try{

               $offer_info = []; 
               $settingsInfo = [];
               $shopInfo = [];

               $input = $request->all();

               $shopify_handle = $input['shopify_handle'];
               $shopify_domain = $input['shopify_domain'];

                $shopifyShop = ShopifyShop::select(\DB::raw('shopify_shops.user_id'))
                ->join('users', 'shopify_shops.user_id', '=' ,'users.id')
                ->where('shopify_shops.domain', $shopify_domain)
                ->orWhere('users.name', $shopify_domain)
                ->first();

               if($shopifyShop){
                  $shop = User::find($shopifyShop->user_id);

                  if($shop){
                   $products_offers = DB::table('up_sells_offers as offer')
                                        ->select('offer.*','offer.title as offer_title','products.*','products.id as product_db_id')
                                       ->join('products', 'products.offer_id', '=', 'offer.id')                                   
                                       ->where('products.handle',$shopify_handle)
                                       ->where('offer.user_id',$shop->id)
                                       ->where('offer.status',"Active")
                                       ->whereNull('offer.deleted_at')
                                       ->get()->toArray();    
                                                                        

                   if(count($products_offers) > 0){ 
                   foreach($products_offers as $key => $offer){

                    $offer_item = [];        

                    $product_db_id = $offer->product_db_id;   
                    
                    $offer_item['name'] = $offer->name; 
                    $offer_item['title'] = $offer->offer_title; 
                    $offer_item['id'] = $offer->offer_id; 
                    $offer_item['status'] = $offer->status; 
                    
                  
                    $upSells =  upSellsProducts::with(['upSellsVariants'])->where('product_db_id',$product_db_id)->get();  
                  
                    $product_info = [];
                    
                   if(count($upSells) > 0){
                          foreach($upSells as $upSellsItem){   
                             $item = [];                       
                             $product_id_1 =  $upSellsItem['product_id_1'];  
                             $query_params_product = ["id" => $product_id_1];
                            // $product = $this->shopify_GetProductByID($shop,$query_params_product);
                            // $item['product'] = $product;
                             $up_sells_variants =  $upSellsItem['upSellsVariants'];  
                             $item['image'] = $upSellsItem['image'];
                             $item['handle'] = $upSellsItem['handle'];
                             $item['id'] = $product_id_1;
                             $item['product_url'] =  "https://".$shopify_domain."/"."products"."/".$upSellsItem['handle'];
                             $item['title'] = $upSellsItem['title'];
                                                                                                 
                             if(count($up_sells_variants) > 0){
                                $variants = [];                            
                                foreach($up_sells_variants as $variant){                             
                                   // $product_id_2 = $variant['product_id_2'];
                                  //  $query_params_variant = ["id" => $product_id_2];                                
                                  //  $variant = $this->shopify_GetVariantByID($shop,$query_params_variant);
                                    $variant_info = [];
                                   // $variant_info = $variant;

                                    $variant_info['id'] = $variant['product_id_2'];
                                    $variant_info['legacyResourceId'] = $variant['shopify_id'];
                                    
                                    $variant_info['price'] = $variant['price'];                               
                                    $variant_info['image'] = $variant['image'];
                                    $selectedOptions = $variant['selectedOptions']; 
                                                                  

                                    $selectedOptions_new = [];
                                    $selectedOptionsPair = [];

                                    foreach($selectedOptions as $key=>$values){
                                      $options_key = array_keys($values);
                                      array_push($selectedOptions_new,[$options_key[0] => $values[$options_key[0]]]);
                                      array_push($selectedOptionsPair,["name" => $options_key[0], "value" => $values[$options_key[0]]]);
                                    }
                                    $variant_info['selectedOptions_new'] = $selectedOptions_new;

                                    $variant_info['selectedOptions'] = $selectedOptionsPair; 
                                    
                                    array_push($variants,$variant_info);  
                                                                    
                                  }    
                                  $item['variants'] = $variants;                                
                                  $options = $this->filterOption($item['variants']);
                                  $item['options'] = $options;   
                             }                                              

                             array_push($product_info,$item);
                          }  
                      }

                      $offer_item['products'] = $product_info;                 
                      array_push($offer_info,$offer_item);
                    }
                   }


                  $settings = Customizations::where('user_id',$shop->id)->select('is_app_enable','settings')->first();  
                if($settings){
                    $settingsInfo = $settings;                         
                }   
                $shop_Info = $this->shopify_getShopInfo($shop);
                if($shop_Info){
                    $shopInfo = $shop_Info;                         
                }  
              }   
          }

          return response()->json([
            'success' => true,     
            'shop' => $shopInfo,  
            'settings' => $settingsInfo,    
            'offers' => $offer_info,                                 
          ],200);  
 
           }catch(Exception $e){
              logger($e);
               return Response::json([
                    "success" => false,
                    "data" => [],
                    "message" => $e->getMessage()
             ],422);
           }        
     }


    public function filterOption($varaints){
               
       $selectedOption = [];
       foreach($varaints as $variant){
            $selectedOption[] = $variant['selectedOptions_new'];
       }
       $optionInfo = []; 
       
       foreach($selectedOption as $key => $options){
          foreach($options as $opt_key => $option){

            $options_key = array_keys($option);

                if(isset($optionInfo[$options_key[0]])){
                  if(!in_array($option[$options_key[0]],$optionInfo[$options_key[0]])){
                    $optionInfo[$options_key[0]][] = $option[$options_key[0]]; 
                  }                    
                }else{
                  $optionInfo[$options_key[0]][] = $option[$options_key[0]];   
                }            
           }        
       }         
       return $optionInfo;
       
    }

    public function AddUpSellsProductsToCart(Request $request){
            try{

              $input = $request->all();

              $variant = $input['variant'];
              $shopfrom = $input['shop'];

              $shop = User::where('id',$shopfrom['id'])->first();

              $params = [
                 "quantity" => 1,
                 "variantId" => $variant['id']
              ];

             $this->shopify_addProductToCart($shop,$params);

            }catch(Exception $e){
                    return Response::json([
                      "success" => false,
                      "data" => [],
                      "message" => $e->getMessage()
              ],422);
            }         
    }
 
    public function AddOfferClick(Request $request){
      try{

        $input = $request->all();

        $offer_id = $input['offer_id'];
        $shopify_domain = $input['shopify_domain'];

        $shopifyShop = ShopifyShop::select(\DB::raw('shopify_shops.user_id'))
                ->join('users', 'shopify_shops.user_id', '=' ,'users.id')
                ->where('shopify_shops.domain', $shopify_domain)
                ->orWhere('users.name', $shopify_domain)
                ->first();
               
        if($shopifyShop){
         $is_exist_offer = upSellsOfferAnalytics::where('offer_id', $offer_id)->where('user_id', $shopifyShop->user_id)->first();
         $db_offer = ( $is_exist_offer ) ? $is_exist_offer : new upSellsOfferAnalytics;
         $db_offer->user_id = $shopifyShop->user_id;    
         $db_offer->offer_id = $offer_id;          
         $db_offer->click_count = $db_offer->click_count + 1;
         $db_offer->save();        
        }

        return response()->json([
          'success' => true                      
     ],200);  

      }catch(Exception $e){
              return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
        ],422);
      }         
}

}
