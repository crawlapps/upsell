<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class MerchantsController extends Controller
{
    public function index(){
        return view('admin.merchants.list');
    }


    public function getMerchants(Request $request)
    {
           ## Read value
           $draw = $request->get('draw');
           $start = $request->get("start");
           $rowperpage = $request->get("length"); // Rows display per page
   
           $columnIndex_arr = $request->get('order');
           $columnName_arr = $request->get('columns');
           $order_arr = $request->get('order');
           $search_arr = $request->get('search');
   
           $columnIndex = $columnIndex_arr[0]['column']; // Column index
           $columnName = $columnName_arr[$columnIndex]['data']; // Column name

           $columnSortOrder = $order_arr[0]['dir']; // asc or desc

           $searchValue = $search_arr['value']; // Search value
   
           // Total records
           $totalRecords = User::select('count(*) as allcount')->count();
           $totalRecordswithFilter = User::select('count(*) as allcount')->where('name', 'like', '%' .$searchValue . '%')->count();
   
           // Fetch records
           $records = User::orderBy($columnName,$columnSortOrder)
                  ->where('name', 'like', '%' .$searchValue . '%')
                 ->select('*')
                 ->skip($start)
                 ->take($rowperpage)
                 ->get();
   
           $data_arr = array();
   
           foreach($records as $record){
              $id = $record->id;
              $name = $record->name;
              $email = $record->email;
              $created_at = $record->created_at;
   
              $data_arr[] = array(
                  "id" => $id,
                  "name" => $name,
                  "email" => $email,
                  "created_at" =>   $created_at->format('Y-m-d H:00')
              );
           }
   
           $response = array(
              "draw" => intval($draw),
              "iTotalRecords" => $totalRecords,
              "iTotalDisplayRecords" => $totalRecordswithFilter,
              "aaData" => $data_arr
           );
   
           return response()->json($response); 
    }


}
