<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\User;
use Hash;
use Auth;
class DashboardController extends Controller
{
    
    public function dashboard()
    {

        $total_merchants =  User::count();

        return view('admin.dashboard',['total_merchants'=>$total_merchants]);
    }


    public function changePassword(){
        return view('admin.pages.change-password');
    }


    public function updatePassword(Request $request){
     

        $request->validate([
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
          ]);

          $input =  $request->all();
           
          $password = $input['password'];

          $email = Auth::guard('admin')->user()->email;

          $user = Admin::where('email', $email)->first();

          if (!Hash::check($request->current_password, $user->password)) {             
               // return back()->with('error', 'Current password does not match!')->withInput();
                return redirect()->back()->withInput()->with('error', 'Current password does not match!');
          }

        $user->password = bcrypt($password);
        $user->save();

        return back()->with('success', 'Password successfully changed!');

    }

}
