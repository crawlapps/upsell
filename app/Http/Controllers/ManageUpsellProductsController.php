<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ShopifyTrait;
use Response;
use Exception;
use Auth;
use App\Models\Products;
use App\Models\upSellsProducts;
use App\Models\upSellsProductVariants;
use App\Models\User;
use App\Models\upSellsOffer;
use DB;
use Symfony\Component\Intl\Currencies;
class ManageUpsellProductsController extends Controller
{

   use ShopifyTrait; 
    
   public function __construct(){

   }

   public function getShopifyProducts(Request $request){
         
    try{

       $shop = Auth::user();        
       
       $productsInfo = [];
            
       $shopInfo = $this->shopify_getShopInfo($shop);
      
       $input = $request->all();

       logger("input");
       logger(json_encode($input));
        
       $page_filter = [];
       if(isset($input['pageFilter'])){
          $page_filter = $input['pageFilter'];
       }

       $result = $this->shopify_GetProducts($shop,$page_filter);

       if($result['products']){
           $productsInfo = $this->createResponseProductsFromQl($result['products']);
       }

       return Response::json([
            'success' => true,
            'domain' => $shop->name,
            'shopInfo'  => $shopInfo,
            'products' => $productsInfo,
            'pageInfo' =>  $result['pageInfo']     
       ],200);     

    }catch(Exception $e){
        return Response::json([
               "success" => false,
               "data" => [],
               "message" => $e->getMessage()
        ],422);
    }
    
   }


   public function getUpSellsProducts(Request $request,$shopify_id){
      try{

         $shop = Auth::user(); 
         $user_id = $shop->id;

         $product_info = []; 

         $Products = Products::with(['upSells','upSells.upSellsVariants'])->where('user_id',$user_id)->where('shopify_id',$shopify_id)->get();

        // dd($Products);

         if(count($Products) > 0){            
             foreach($Products as $product){                  
                  $upSells =  $product['upSells'];
                 
                  if(count($upSells) > 0){
                     foreach($upSells as $upSellsItem){
                     $item = [];
                     $item['id'] =  $upSellsItem['product_id_1'];  
                     $item['variants'] = [];    
                     $up_sells_variants =  $upSellsItem['upSellsVariants'];    
                     if(count($up_sells_variants) > 0){
                        $variants = [];
                        foreach($up_sells_variants as $variant){
                            array_push($variants,['id'=>$variant['product_id_2']]);
                        }
                        $item['variants'] = $variants;
                     }                     
                     array_push($product_info,$item);
                  } 
               }  
             }                          
         }

         return Response::json([
            'success' => true,            
            'products' => $product_info            
       ],200);  

      }catch(Exception $e){
            return Response::json([
                     "success" => false,
                     "data" => [],
                     "message" => $e->getMessage()
            ],422);
      }
   }

   public function attachUpSellsProducts(Request $request){
       try{

          $input = $request->all();
         
          $shop = Auth::user(); 
          $user_id = $shop->id;
          $parent_product = $input['parent_product']; 
          $shopify_id =  $parent_product['id'];
          $upSellProductsSelection = $input['upsell_products_selection'];
          $handle = $parent_product['handle'];
     
          Products::with(['upSells','upSells.upSellsVariants'])->where('user_id',$user_id)->where('shopify_id',$shopify_id)->delete();
          Products::where('user_id',$user_id)->where('shopify_id',$shopify_id)->delete();
            
          if(count($upSellProductsSelection) > 0){

            $product = Products::firstOrCreate([
               "user_id" => $user_id,
               "shopify_id" => $shopify_id,
               "handle" => $handle
           ]);

            $product_db_id = $product['id'];

             foreach($upSellProductsSelection as $key=>$selection){
                               
              $upsells_product =  upSellsProducts::firstOrCreate([
                       "product_db_id" => $product_db_id,
                       "product_id_1" => $selection['id']                       
               ]);

               $variantsSelection =  $selection['variants'];               

               if(count($variantsSelection) > 0){

                  $product_id_1 = $upsells_product['id'];

                  foreach($variantsSelection as $key=>$variant){

                  upSellsProductVariants::firstOrCreate(
                        [
                        "product_id_1" =>  $product_id_1,
                        "product_id_2" =>  $variant['id']
                        ]
                     );
                  }
               }
             }              
          }

          return  Response::json(['success' => true, 'message' => 'Upsells attached successfully.'], 200);

       }catch(Exception $e){                
         return Response::json([
            "success" => false,
            "data" => [],
            "message" => $e->getMessage()
         ],422);
       }
   }

  
   public function getOffer(Request $request){
      try{

         $input = $request->all();
         $total_offers_records = 0;
         $shop = Auth::user();
         $user_id = $shop->id; 
         $shopInfo = $this->shopify_getShopInfo($shop);  

         $offerQuery = upSellsOffer::select('*')->where('user_id',$user_id);
       
         $total_offers_query = $offerQuery;
         $total_offers = $offerQuery->get();
         $total_offers_records = count($total_offers);

         //================================================================================================================
         //Filter 
         //================================================================================================================

              $filterData = $input['filterData'];

               //filter For search title,name
                  if(isset($filterData['search']) && $filterData['search']!=="" && $filterData['search'] !==null){
                     $filter_by_search = $filterData['search'];
                     $offerQuery->where('title','LIKE', "%$filter_by_search%")->orWhere('name','LIKE', "%$filter_by_search%");
                  }
                  //filter For Status
                  else if(isset($filterData['filterByStatus']) && $filterData['filterByStatus']!=="" && $filterData['filterByStatus'] !==null && $filterData['filterByStatus'] !=="0"){
                     $filter_by_type = $filterData['filterByStatus'];
                     $offerQuery->where('status','LIKE', "%$filter_by_type%");            
                  }
                  //filter For SORT-------------------
                 else if(isset($filterData['filterBySort']) && $filterData['filterBySort']!=="" && $filterData['filterBySort'] !==null){
               
                  $filter_by_sort = $filterData['filterBySort'];

                  if($filter_by_sort=="titleAlpha"){

                     //=== Offer title A–Z===
                        $offerQuery->orderBy('name','ASC');

                  }else if($filter_by_sort=="titleReverseAlpha"){

                        //===Offer title Z–A===
                        $offerQuery->orderBy('name','desc');

                  }else if($filter_by_sort=="oldestCreated"){

                        //===Created (oldest first)===
                        $offerQuery->orderBy('created_at', 'ASC');

                  }else if($filter_by_sort=="newestCreated"){  

                        //===Created (newest first)===
                        $offerQuery->orderBy('created_at', 'desc');
                  }
                  else{
                        $offerQuery->orderBy('created_at', 'desc');
                       // $offerQuery->orderBy('name','ASC');
                  }
               }         
         //================================================================================================================
         //@end :: Filter 
         //================================================================================================================
         

         $offersRecs = $offerQuery->paginate(10);

         $offers_response = [];

         if(count($offersRecs)>0){
              foreach($offersRecs as $key=>$value){
                 $item = [];
                 $item = $value;
                 $offer_id = $value->id;

                 $products = DB::table('products')
                           ->join('up_sells_products', function ($join) {
                              $join->on('products.id', '=', 'up_sells_products.product_db_id');                                  
                           }) 
                           ->where('products.offer_id',$offer_id)
                           ->pluck('up_sells_products.shopify_id')
                           ->toArray();

                if(count($products) > 0){
                                           
                 $revenue_query = DB::table('orders') 
                 ->join('line_items', function ($join) use($products){
                     $join->on('orders.id', '=', 'line_items.db_order_id'); 
                     $join->whereIn('product_id',$products);                                    
                 })
                 ->where('orders.user_id',$user_id)
                 ->select(DB::raw('SUM(line_items.price*line_items.quantity) AS price_count'));
                   $revenue_data = $revenue_query->first(); 
                   
                   if($revenue_data){
                     $price_count = $revenue_data->price_count ? number_format($revenue_data->price_count,2) : 0;
                    // $total_revenue = number_format_short($price_count);
                     $item['price_count']  =  $price_count;    
                    } 
                 }
             }
         }

         $data['pagination'] = [
            'previousPageUrl' => $offersRecs->previousPageUrl(),
            'nextPageUrl' => $offersRecs->nextPageUrl(),
         ];

         $data['offers'] = $offersRecs;
         $currency_symbol = Currencies::getSymbol($shopInfo->currency);
        return Response::json([
            'success' => true,
            'domain' => $shop->name,
            'shopInfo'  => $shopInfo, 
            "currency_symbol" => $currency_symbol, 
            'total_offers' => $total_offers_records,      
            "data" => $data           
       ],200); 

      }catch(Exception $e){
         return Response::json([
            "success" => false,          
            "message" => $e->getMessage()
         ],422);
      }
   }

  public function addOffer(Request $request){
     try{
      
      $input = $request->all();
    
      $shop = Auth::user(); 
      $user_id = $shop->id;   
      $form = $input['form'];

      $offer_name =  $form["offer_name"]; 
             
      $existOffer = upSellsOffer::where('user_id',$user_id)->where('name',$offer_name)->get();
      if(count($existOffer) > 0){
         return  Response::json(['success' => false, "error_name" =>"offer_name", 'message' => 'Offer name already exists'], 200);
      }  

      $offer_title = $form["offer_title"];
      $offer_status = $form["offer_status"];

      $target_products = $input["target_products"];
      $upsell_products = $input["upsell_products"];

      $offer = upSellsOffer::firstOrCreate([
         "user_id" => $user_id,
         "name" => $offer_name,
         "title" => $offer_title,
         "status" =>  $offer_status
      ]);
             
       $offer_id = $offer['id'];

       if(count($target_products) > 0){

         foreach($target_products as $key=>$parent_product){
         $shopify_gid_path = $parent_product['id'];
         $handle = $parent_product['handle'];
         $shopify_id = gidToShopifyId($shopify_gid_path);

         $product = Products::firstOrCreate([
            "offer_id" => $offer_id,
            "shopify_id" => $shopify_id,
            "handle" => $handle,
            "gid" => $parent_product['id'],
            "image" => $parent_product['image'],
            "title" => $parent_product['title']
        ]);

        $product_db_id = $product['id'];

       if(count($upsell_products) > 0){        

          foreach($upsell_products as $key=>$selection){
                                        
           $shopify_id_1 = gidToShopifyId($selection['id']);

          
           $upsells_product =  upSellsProducts::firstOrCreate([
                    "product_db_id" => $product_db_id,
                    "product_id_1" => $selection['id'],
                    "shopify_id" => $shopify_id_1,  
                    "handle" => $selection['handle'],
                    "user_id" => $user_id,                  
                    "image" => $selection['image'],
                    "title" => $selection['title']              
            ]);

            $variantsSelection =  $selection['variants'];  
            
            $options = $selection['options'];

            if(count($variantsSelection) > 0){

               $product_id_1 = $upsells_product['id'];

               foreach($variantsSelection as $key=>$variant){

                  $shopify_id_2 = gidToShopifyId($variant['id']);
                  
                  $varaint_image = isset($variant['image']['originalSrc']) ? $variant['image']['originalSrc'] : $selection['image'];

                  $map_options = [];
                  $selectedOptions = $variant['selectedOptions'];
                  if(count($selectedOptions) > 0){
                     foreach($selectedOptions as $key=>$option){
                              array_push($map_options,[$options[$key]['name'] => $option['value']]);
                     }
                  }
                     
               upSellsProductVariants::firstOrCreate(
                     [
                        "product_id_1" =>  $product_id_1,
                        "product_id_2" =>  $variant['id'],
                        "shopify_id" => $shopify_id_2,
                        "image" => $varaint_image,
                        "title" => $variant['title'],
                        "price" =>  $variant['price'],
                        "selectedOptions" =>  $map_options
                     ]
                  );
               }
            }
          }              
       }
     }
   }

     return  Response::json(['success' => true, 'message' => 'Offer Created'], 200);

     }catch(Exception $e){
         return Response::json([
            "success" => false,            
            "message" => $e->getMessage()
         ],422);
     }
  }


  public function addOfferNameCheck(Request $request){
    
   try{

      $shop = Auth::user(); 
      $user_id = $shop->id;  

      $input = $request->all();

      $offer_name = $input['offer_name'];

      $existOffer = upSellsOffer::where('user_id',$user_id)->where('name',$offer_name)->get();
      if(count($existOffer) > 0){
         return  Response::json(['success' => true, 'message' => 'Offer name already exists'], 200);
      }      
      return  Response::json(['success' => false], 200);
      

   }catch(Exception $e){
       return Response::json([
           "success" => false,
           "message" => $e->getMessage()             
       ]);
   }

}


  public function deleteOffers(Request $request){
    
      try{

         $shop = Auth::user(); 
         $user_id = $shop->id;  

         $input = $request->all();

         $offer_ids = $input['offer_ids'];

         $offer_ids = json_decode($input['offer_ids'],true);

         $offerDelete = upSellsOffer::whereIn('id',$offer_ids)->where('user_id',$user_id)->delete();

         return  Response::json(['success' => true, 'message' => 'Offer delete successfully.'], 200);
         

      }catch(Exception $e){
          return Response::json([
              "success" => false,
              "message" => $e->getMessage()             
          ]);
      }

  }

  public function changeStatusForOffers(Request $request){
       
      try{

         $shop = Auth::user(); 
         $user_id = $shop->id;  

         $input = $request->all();
         $status = $input['status'];
         $offer_ids = json_decode($input['offer_ids'],true);
        
         $offerUpdate = upSellsOffer::whereIn('id',$offer_ids)->where('user_id',$user_id)->update([
             "status" => $status
         ]);          

         return  Response::json(['success' => true, 'message' => 'Offer status change successfully.'], 200);

      }catch(Exception $e){
         return Response::json([
            "success" => false,
            "message" => $e->getMessage()  
         ]);
      }
    
   }

   public function editOffer(Request $request,$id){
      try{
         
         if($id){

         $offerInfo = [];   
         $targetProductsInfo = [];
         $upsellProductsInfo = [];
         $targetProductPreviewInfo = [];
         $upsellProductsPreviewInfo = [];

         $offerQuery = upSellsOffer::select('*')->where('id',$id)->with('products')->first();
         if($offerQuery){   

            $offerInfo['offer_name']  = $offerQuery->name;
            $offerInfo['offer_title']  = $offerQuery->title;
            $offerInfo['offer_status']  = $offerQuery->status;
            
            $user_id = $offerQuery->user_id;
          
            if(isset($offerQuery->products)){
               $target_products =  $offerQuery->products;
                 if(count($target_products) > 0 ){                      
                     foreach($target_products as $key=>$parent_product){

                        $shopify_gid_path = $parent_product['gid'];

                        array_push($targetProductsInfo,['id' => $shopify_gid_path]);
                         
                        $targetProductPreviewItem = [
                           "id" => $shopify_gid_path,
                           "title" => $parent_product['title'],
                           "handle" => $parent_product['handle'],
                           "image" => $parent_product['image']
                        ];                                            

                        array_push($targetProductPreviewInfo,$targetProductPreviewItem);
                        if( $key == '0'){
                        $product_db_id = $parent_product['id'];

                        $upSells = upSellsProducts::with(['upSellsVariants'])->where('user_id',$user_id)->where('product_db_id',$product_db_id)->get();
                 
                        if(count($upSells) > 0){

                           foreach($upSells as $upSellsItem){
                           $item = [];
                           $variantsPreviewData = [];
                           $item['id'] =  $upSellsItem['product_id_1'];  
                           $item['variants'] = [];    
                           $up_sells_variants =  $upSellsItem['upSellsVariants']; 
                                                     
                       
                           if(count($up_sells_variants) > 0){
                              $variants = [];
                              $variants_preview_item = [];
                              foreach($up_sells_variants as $variant){
                                  array_push($variants,['id'=>$variant['product_id_2']]);

                                  $selectedOptions = $variant['selectedOptions']; 

                                  $selectedOptionsPair = [];

                                  foreach($selectedOptions as $key=>$values){
                                    $options_key = array_keys($values);                                   
                                    array_push($selectedOptionsPair,["value" => $values[$options_key[0]]]);
                                 }
                                                              
                                  $variants_preview_item = [
                                    "id" => $variant['product_id_2'],
                                    "title" => $variant['title'],
                                    "image" => [
                                       "originalSrc" => $variant['image']
                                    ],  
                                    "price" => $variant['price'],
                                    "selectedOptions" => $selectedOptionsPair
                                  ];

                                  array_push($variantsPreviewData,$variants_preview_item);
                              }
                              $item['variants'] = $variants;                            
                           }  
                           
                        
                           array_push($upsellProductsInfo,$item);

                           $options = $this->filterOption($up_sells_variants);

                           $upsellProductPreviewItem = [
                              "id" => $upSellsItem['product_id_1'],
                              "title" => $upSellsItem['title'],
                              "image" => $upSellsItem['image'],
                              "handle" => $upSellsItem['handle'],
                              "variants" => $variantsPreviewData,
                              "options" => $options
                           ];                                            
                              array_push($upsellProductsPreviewInfo,$upsellProductPreviewItem);
                          }
                        } 
                      }  
                   }   
                }
            }
         }
            return Response::json([
               'success' => true, 
               'offer' => $offerInfo,
               'target_products' => $targetProductsInfo,
               'target_products_preview_data' => $targetProductPreviewInfo,
               'upsell_products' => $upsellProductsInfo,
               'upsell_products_preview_data' => $upsellProductsPreviewInfo,
         ],200); 

         }   

      }catch(Exception $e){
         return Response::json([
             "success" => false,
             "message" => $e->getMessage()
         ]);
      }
   }

   public function filterOption($varaints){
               
      $selectedOption = [];
      foreach($varaints as $variant){
           $selectedOption[] = $variant['selectedOptions'];
      }
      $optionInfo = []; 
      
      foreach($selectedOption as $key => $options){
         foreach($options as $opt_key => $option){

           $options_key = array_keys($option);
          
         
           if(isset($optionInfo[$opt_key]['name'])){
                    if($optionInfo[$opt_key]['name']==$options_key[0]){
                        if(!in_array($option[$options_key[0]],$optionInfo[$opt_key]['values'])){
                              $optionInfo[$opt_key]['values'][]  = $option[$options_key[0]];
                        }
                    }
           }else{
               $values = [];
               $values[] = $option[$options_key[0]];
               array_push($optionInfo,['name' => $options_key[0], "values" => $values]); 
           }        

               // if(isset($optionInfo[$key][$options_key[0]])){                 
               //   if(!in_array($option[$options_key[0]],$optionInfo[$key]['value'])){
               //     $optionInfo[$key]['value'][] = $option[$options_key[0]];                    
               //   }                    
               // }else{
               //    array_push($optionInfo,['name' => $options_key[0], "value" => $option[$options_key[0]]]); 
               // }            
          }        
      }           
      
      return $optionInfo;
      
   }


   public function updateOffer(Request $request,$id){
      try{

           $input = $request->all(); 
             
           $form = $input['form'];

           $offer = upSellsOffer::find($id);
           $user_id = $offer['user_id'];

           $offer_name =  $form["offer_name"];

            $existOffer = upSellsOffer::where('user_id',$user_id)->where('name',$offer_name)->where('id', '!=' , $id)->get();

            if(count($existOffer) > 0){
               return  Response::json(['success' => false, "error_name" =>"offer_name", 'message' => 'Offer name already exists'], 200);
            }  
            
            $offer_title = $form["offer_title"];
            $offer_status = $form["offer_status"];

            $target_products = $input["target_products"];
            $upsell_products = $input["upsell_products"];
            
            $offer->name = $offer_name;
            $offer->title =  $offer_title;
            $offer->status =  $offer_status;
            $offer->save();  

            $offer_id = $offer['id'];
           

            $delete_product = Products::where('offer_id',$offer_id)->forceDelete();

            if(count($target_products) > 0){

               foreach($target_products as $key=>$parent_product){
               $shopify_gid_path = $parent_product['id'];
               $handle = $parent_product['handle'];
               $shopify_id = gidToShopifyId($shopify_gid_path);

               $product = Products::firstOrCreate([
                  "offer_id" => $offer_id,
                  "shopify_id" => $shopify_id,
                  "handle" => $handle,
                  "gid" => $parent_product['id'],
                  "image" => $parent_product['image'],
                  "title" => $parent_product['title']
            ]);

            $product_db_id = $product['id'];

            if(count($upsell_products) > 0){        

               foreach($upsell_products as $key=>$selection){
                                             
               $shopify_id_1 = gidToShopifyId($selection['id']);

               $upsells_product =  upSellsProducts::firstOrCreate([
                        "product_db_id" => $product_db_id,
                        "product_id_1" => $selection['id'],
                        "shopify_id" => $shopify_id_1,  
                        "handle" => $selection['handle'],
                        "user_id" => $user_id,                  
                        "image" => $selection['image'],
                        "title" => $selection['title']              
                  ]);

                  $variantsSelection =  $selection['variants'];   
                  
                  $options = $selection['options'];

                  if(count($variantsSelection) > 0){

                     $product_id_1 = $upsells_product['id'];

                     foreach($variantsSelection as $key=>$variant){

                        $shopify_id_2 = gidToShopifyId($variant['id']);
                        
                        $varaint_image = isset($variant['image']['originalSrc']) ? $variant['image']['originalSrc'] : '/images/defaultProduct.png';
                       
                        $map_options = [];
                        $selectedOptions = $variant['selectedOptions'];
                        if(count($selectedOptions) > 0){
                           foreach($selectedOptions as $key=>$option){
                                    array_push($map_options,[$options[$key]['name'] => $option['value']]);
                           }
                        }
                             
                     upSellsProductVariants::firstOrCreate(
                           [
                              "product_id_1" =>  $product_id_1,
                              "product_id_2" =>  $variant['id'],
                              "shopify_id" => $shopify_id_2,
                              "image" => $varaint_image,
                              "title" => $selection['title'],
                              "price" =>  $variant['price'],
                              "selectedOptions" =>  $map_options 
                           ]
                        );
                     }
                  }
               }              
            }
         }
       }

     return  Response::json(['success' => true, 'message' => 'Offer Updated'], 200);

      }catch(Exception $e){
         return Response::json([
             "success" => false,
             "message" => $e->getMessage()
         ]);
      }
   }



}
