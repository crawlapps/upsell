<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;
use App\Models\User;
use DB;
use Exception;

use App\Events\CheckProductsUpdate;

class ProductsUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            logger("===============START :: Product Update Job ===============");

          
            $shop = User::where('name', $this->shopDomain)->first();

            if($shop){

                event (new CheckProductsUpdate($shop->id,$this->data));
                
                return response()->json(["data" => "success"],200);
               
            }

            return response()->json(["data" => "success"],200);

            logger("===============END :: Product Update Job ===============");


        // Convert domain
       // $this->shopDomain = ShopDomain::fromNative($this->shopDomain);

        // Do what you wish with the data
        // Access domain name as $this->shopDomain->toNative()
        }catch(Exception $e){
            logger("===============ERROR :: Product Update Job ===============");
            logger(json_encode($e->getMessage()));
        }
    }
}
