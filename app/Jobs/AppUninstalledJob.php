<?php

namespace App\Jobs;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AppUninstalledJob  implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $shopDomain;
    public $data;

    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    public function handle()
    {
        logger("=============== App-Uninstalled :: Job ===============");

        logger($this->shopDomain);

        $user = User::where('name', $this->shopDomain)->first();

        logger($user);

        $user->password = '';
        $user->save();

        $user->delete();

        // event(new CheckOrder($user->id, $this->data));
        return response()->json(['data' => 'success'], 200);
    }

}
