<?php



if(!function_exists('gidToShopifyId')){

    function gidToShopifyId($gid){
        $IdStr = $gid;
        $IdParts = explode('/', $IdStr);
        $shopifyId = end($IdParts);

        return $shopifyId;
    }

}

if(!function_exists('number_format_short')){
    function number_format_short($n)
    {
      if ($n < 1000) return $n;
      $suffix = ['','k','M','G','T','P','E','Z','Y'];
      $power = floor(log($n, 1000));
      return round($n/(1000**$power),1,PHP_ROUND_HALF_EVEN).$suffix[$power];
    };
}