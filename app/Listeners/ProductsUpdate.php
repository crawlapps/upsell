<?php

namespace App\Listeners;

use App\Events\CheckProductsUpdate;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Exception;
use App\Models\User;
use DB;
use App\Models\upSellsProducts;
use App\Models\upSellsProductVariants;
use App\Traits\ShopifyTrait;

class ProductsUpdate
{


    use ShopifyTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CheckProductsUpdate  $event
     * @return void
     */
    public function handle(CheckProductsUpdate $event)
    {

       try{

        logger("============START::Listener:: Product Update===========================");

        $ids = $event->ids;
        $user_id = $ids['user_id'];
        $data = $ids['data'];
      
        logger("data");
        logger(json_encode($data));

        $sh_productID = $data->id;  

        $shop = User::where('id',$user_id)->first();  

        $image_url = isset($data->image->src) ? $data->image->src : '';  

        $upsells_product = upSellsProducts::where('shopify_id', $sh_productID)->where('user_id', $user_id)->get();  

        logger("upsells_product");
        logger(json_encode($upsells_product));

        if(count($upsells_product) > 0){
            foreach($upsells_product as $products){

                logger("products");
                logger(json_encode($products));

                $product_id_1 = $products['id'];    

                logger("$product_id_1");
                logger(json_encode($product_id_1));
                        
                $variants  = upSellsProductVariants::where('product_id_1', $product_id_1)->get();  

                logger("$variants");
                logger(json_encode($variants));


                if(count($variants) > 0){
                    foreach ( $variants as $variant ){

                         $product_id_2 = $variant['product_id_2'];

                         $query_params_variant = ["id" => $product_id_2];  

                         logger("query-params-variants");
                         logger($query_params_variant);

                         $variant_res = $this->shopify_GetVariantByID($shop,$query_params_variant);
                        
                         logger("variant_res");
                         logger(json_encode($variant_res));

                         if($variant_res){ 

                         $variant_price = $variant_res['price']; 

                          logger("variant_res");
                         logger(json_encode($variant_res));  

                         $variant_image = isset($variant_res['image']['originalSrc']) ? $variant_res['image']['originalSrc'] : $image_url;

                         $variant_title = $variant_res['title']; 

                         $db_variant = upSellsProductVariants::where('id', $variant['id'])                                             
                                         ->update([
                                                   'price' => $variant_price,
                                                   'image' => $variant_image,
                                                   'title' => $variant_title
                                                  ]);
                         }
                    }  
                }  
            }    
                   

            $db_upsells_product =  upSellsProducts::where('shopify_id', $sh_productID)->where('user_id', $user_id)
                                    ->update([                                               
                                            'image' => $image_url,
                                            'title' => $data->title
                                            ]);

        } 


        }catch(Exception $e){

            logger("==============ERROR::Listener:: Product Update=======================");
            logger($e->getMessage());

        }
    }
}
