<?php

namespace App\Listeners;

use App\Events\CheckOrder;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\upSellsProducts;
use App\Models\Order;
use App\Models\LineItem;
use App\Models\Customer;
class OrderCreateUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CheckOrder  $event
     * @return void
     */
    public function handle(CheckOrder $event)
    {
        try{
         
         logger("============START::Listener:: Order create Update===========================");
         
          $ids = $event->ids;
          $user_id = $ids['user_id'];
          $data = $ids['data'];

          logger("data-id => ".$data->id);
          
          //$id = gidToShopifyId($data->admin_graphql_api_id);
          
       //   logger("order.id =>".$id);

          logger("user_id => ".$user_id);    
          
          $sh_product_ids = [];
          $sh_lineItems = [];


          if( !empty( $data->line_items ) ){
            $sh_lineItems = $data->line_items;

            foreach ( $sh_lineItems as $lkey=>$lval ){
                $sh_product_ids[] = $lval->product_id;
            }
         }

         $is_exist_product = upSellsProducts::where('user_id', $user_id)->whereIn('shopify_id', $sh_product_ids)->count();

         if($is_exist_product > 0){

            $is_exist_order = Order::where('shopify_order_id', $data->id)->where('user_id', $user_id)->first();
            $db_order = ( $is_exist_order ) ? $is_exist_order : new Order;
            $db_order->user_id = $user_id;
            $db_order->shopify_order_id = $data->id;
            $db_order->name = $data->name;
            $db_order->fulfillment_status = (@$data->fulfillment_status) ? $data->fulfillment_status : 'Unfulfilled';
            $db_order->full_response = $data;
            $db_order->save();

            
            if(isset($data->customer)){
             
             $customer = $data->customer;   
                
             $is_exist_customer = Customer::where('shopify_customer_id', $customer->id)->where('user_id', $user_id)->first();
             $db_customer = ( $is_exist_customer ) ? $is_exist_customer : new Customer;
             $db_customer->user_id = $user_id;
             $db_customer->shopify_customer_id = $customer->id;
             $db_customer->email = $customer->email;
             $db_customer->accepts_marketing = $customer->accepts_marketing;
             $db_customer->verified_email = $customer->verified_email;
             $db_customer->currency = $customer->currency;
             $db_customer->admin_graphql_api_id = $customer->admin_graphql_api_id;
             $db_customer->default_address = $customer->default_address;
             $db_customer->upsell_orders_count = $db_customer->upsell_orders_count + 1;
             $db_customer->save();

            }
             

            if( !empty( $sh_lineItems ) ){
                foreach ( $sh_lineItems as $lkey=>$lval ) {

                    $sh_product_id = $lval->product_id;
                    $exist_p = upSellsProducts::where('shopify_id', $sh_product_id)->where('user_id', $user_id)->first();
                    if ($exist_p) {

                        $is_exist_lineItem = LineItem::where('db_order_id', $db_order->id)->where('shopify_lineitem_id',
                        $lval->id)->first();

                        $lineItem = LineItem::firstOrNew([
                            'shopify_lineitem_id' => $lval->id, 'db_order_id' => $db_order->id
                        ]);

                        $lineItem->db_order_id = $db_order->id;
                        $lineItem->shopify_lineitem_id = $lval->id;
                        $lineItem->name = $lval->name;
                        $lineItem->price = $lval->price;
                        $lineItem->product_id = $lval->product_id;
                        $lineItem->variant_id = $lval->variant_id;                       
                        $lineItem->quantity = $lval->quantity;
                        $db_order->full_response = $lval;
                        $lineItem->save();

                    }
                }
            }
         }else{
            $db_order = Order::where('shopify_order_id', $data->id)->where('user_id', $user_id)->get();
            if( count($db_order ) > 0 ){
                foreach ( $db_order as $dkey=>$dval ){
                    $lineItems = LineItem::where('db_order_id', $dval->id)->get();
                    if( count( $lineItems ) > 0 ){
                        foreach ( $lineItems as $lkey=>$lval ){
                            $lval->delete();
                        }
                    }
                    $dval->delete();
                }
            }
        }
                              
          logger("============END::Listener:: Order create Update===========================");

        }catch(Exception $e){

            logger("==============ERROR::Listener:: Order create Update=======================");
            logger($e->getMessage());

        }
    }
}
