<?php

namespace App\Traits;
use Exception;
use Response;
use App\Models\Order;
use App\Models\LineItem;
use App\Models\upSellsProducts;
use DB;
use App\Models\Customer;
use App\Models\upSellsOfferAnalytics;
use Carbon\CarbonPeriod;
trait AnalyticsTrait{
         
     //=======================================================================================================
     //Total Order
     //=======================================================================================================
         public function analytics_getTotalOrderInfo($user,$filter_data){  
            
                    $total_order = 0;   
             
                    $new_query =  Order::where('user_id',$user->id);

                    if(isset($filter_data) && isset($filter_data['start_date']) && isset($filter_data['end_date'])){
                        
                         $startDate = $filter_data['start_date'];
                         $endDate = $filter_data['end_date'];
                         $new_query = $new_query->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate);
                    }

                    $result = $new_query->count();
                    if($result){
                          $total_order = number_format_short($result);
                    }

                    return $total_order;            
         }
     //@end :: Total Order 


   
     //=======================================================================================================
     //Total Revenue
     //=======================================================================================================
     public function analytics_getTotalRevenueInfo($user,$filter_data){   
      $total_revenue = 0;
      $new_query = DB::table('orders')  
                     ->select(DB::raw('sum(line_items.price*line_items.quantity) AS total_sales'))                   
                     ->join('line_items', function ($join) {
                         $join->on('orders.id', '=', 'line_items.db_order_id');                                  
                     })->where('orders.user_id',$user->id);         

        if(isset($filter_data) && isset($filter_data['start_date']) && isset($filter_data['end_date'])){
                        
            $startDate = $filter_data['start_date'];
            $endDate = $filter_data['end_date'];

            $new_query = $new_query->where('orders.created_at', '>=', $startDate)->where('orders.created_at', '<=', $endDate);

       }

       $result = $new_query->value('total_sales');
       $result = number_format($result,2);

         if($result){
                $total_revenue = number_format_short($result);
            }

        return $total_revenue;  

        }
     //@end :: Total Revenue 


     //=======================================================================================================
     //Total Upsell Products
     //=======================================================================================================
     public function analytics_getTotalUpsellProductInfo($user,$filter_data){   
        
        $total_upsell_product = 0;
         
        $new_query =  upSellsProducts::where('user_id',$user->id);

        if(isset($filter_data) && isset($filter_data['start_date']) && isset($filter_data['end_date'])){
                        
            $startDate = $filter_data['start_date'];
            $endDate = $filter_data['end_date'];

            $new_query = $new_query->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate);

       }

        $result = $new_query->count();
        if($result){
             $total_upsell_product = number_format_short($result);
        }

        return $total_upsell_product;  
                  
        }      
        

     //@end :: Total  Upsell Products


     //=======================================================================================================
     //Total Customer
     //=======================================================================================================
     public function analytics_getTotalCustomerInfo($user,$filter_data){  
         
        $total_customer = 0;
          
        $new_query =  Customer::where('user_id',$user->id);

            $result = $new_query->count();
            if($result){
                $total_customer = number_format_short($result);
            }

         return $total_customer;
                  
        }
     //@end :: Total  Customer


     //=======================================================================================================
     //Total  offers clicked
     //=======================================================================================================
     public function analytics_getTotalOfferClickedInfo($user,$filter_data){   

        $total_offer_clicked = 0;
          
        $new_query =  upSellsOfferAnalytics::where('user_id',$user->id);

        if(isset($filter_data) && isset($filter_data['start_date']) && isset($filter_data['end_date'])){
                        
            $startDate = $filter_data['start_date'];
            $endDate = $filter_data['end_date'];

            $new_query = $new_query->where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate);

       }

        $result = $new_query->sum('click_count');
        if($result){
            $total_offer_clicked = number_format_short($result);
        }

        return $total_offer_clicked;  
                  
        }
     //@end :: Total offers clicked
     

     //==============================================Graph===================================================
     //=======================================================================================================
     // Revenue Graph
     //=======================================================================================================
     public function analytics_RevenueGraphInfo($user,$filter_data){   

           $response_data = [];

            $new_query = DB::table('orders')                     
            ->join('line_items', function ($join) {
                $join->on('orders.id', '=', 'line_items.db_order_id');                                  
            })->where('orders.user_id',$user->id);

            $new_query = $new_query->select('orders.id','orders.created_at',                              
                                DB::raw('DATE(orders.created_at) as date'),
                                DB::raw('MONTH(orders.created_at) as month'),
                                DB::raw('YEAR(orders.created_at) as year'),
                                DB::raw('SUM(line_items.price*line_items.quantity) AS price_count'),
                                DB::raw("(DATE_FORMAT(orders.created_at, '%d-%m-%Y')) as my_date"),
                                DB::raw("(DATE_FORMAT(orders.created_at, '%d-%b-%Y')) as preview_date")
                            );

            $dates = [];     

            if(isset($filter_data) && isset($filter_data['start_date']) && isset($filter_data['end_date'])){
                            
                $startDate = $filter_data['start_date'];
                $endDate = $filter_data['end_date'];
            
                $period = CarbonPeriod::create($startDate,$endDate);
                         
                    // Iterate over the period
                    foreach ($period as $date) {
                        $date_formated = $date->format('d-M-Y');
                        array_push($dates,$date_formated);
                    }
                   
                    // Convert the period to an array of dates
                   
                    $new_query = $new_query->where('orders.created_at', '>=', $startDate)->where('orders.created_at', '<=', $endDate);

                }

            $result = $new_query->groupBy('date')->orderBy('date', 'DESC')->get();     
            
            $x_categories = [];
            $y_series = [];

            if($result){

                $result_sort = [];

                foreach ($result as $key => $row) {
                    $result_sort[$row->preview_date] = $row->price_count;          
                  }

          
                if(count($dates)>0){
                    foreach ($dates as $date) { 
                                                            
                                    if(array_key_exists($date,$result_sort)){

                                            array_push($x_categories,$date);
                                            array_push($y_series,$result_sort[$date]);                                        

                                    }else{
                                        
                                        array_push($x_categories,$date);
                                        array_push($y_series,0);
                                    
                                    }                                                      
                                                
                    }
                }
            }      

            $response_data['x_categories'] = $x_categories;
            $response_data['y_series'] = $y_series;

            return $response_data;

            

        }
     //@end :: Revenue Graph
     
     
       
  
     //=======================================================================================================
     // Top products added to cart
     //=======================================================================================================
     public function analytics_TopCartProductInfo($user){   
        $result =  [];     
        $products =  upSellsProducts::where('user_id',$user->id)
                                    ->select('shopify_id','title','image')
                                    ->groupBy('shopify_id')
                                    ->orderBy('updated_at', 'desc')
                                    ->get()
                                    ->toArray();
            if(count($products)){               

                $upsell_products = [];
                $product_info = [];

                        foreach($products as $key=>$product){

                          $item = [];  

                          $item['title'] = $product['title'];
                          $item['image'] = $product['image'];
                          $item['product_id'] = $product['shopify_id'];
                                 
                        $product_id = $product['shopify_id'];

                        $new_query = DB::table('orders')                     
                            ->join('line_items', function ($join) use($product_id){
                                $join->on('orders.id', '=', 'line_items.db_order_id');
                                $join->where('product_id',$product_id);   
                                $join->groupBy('line_items.product_id'); 
                                $join->groupBy('line_items.db_order_id');                            
                            })        
                           ->select('orders.id',                                
                                 DB::raw('COUNT(orders.id) AS order_count'),
                                 DB::raw('SUM(line_items.price*line_items.quantity) AS price_count')
                                )
                          
                        ->where('orders.user_id',$user->id)->first();   
                       
                            if($new_query){
                                  $item['order_count'] = $new_query->order_count;
                                  $item['price_count'] = number_format($new_query->price_count,2);
                                   if($new_query->order_count > 0){
                                   array_push($product_info,$item);
                               }
                            }                                             
                       
                        }  
                 $result = $product_info;             
                
            }    

        return $result;  

        }
     //@end :: Top products added to cart

}

