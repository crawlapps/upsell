<?php
namespace App\Traits;

use App\Models\ShopifyShop;
use Exception;
use Response;
trait ShopifyTrait {
      
     public function shops(){

         $shops = ShopifyShop::all();
         return $shops;

          //REST API EXAMPLE
          // $shopInof = $shop->api()->rest('GET','/admin/shop.json')['body']['shop'];
          // return $shopInof;

          // GraphQL API EXAMPLE

        //   $query = '{ shop { name } }';

        //   $shopRequest =  $shop->api()->graph($query); 
        //   if(!$shopRequest['errors']){
        //       $shops = $shopRequest['body']['data']['shop']['name'];
        //       dd($shops);
        //   } 
          
          
     }

 
     public function shopify_getShopInfo($shop){
          $shopInfo = [];
          try{

            $shopRequest = $shop->api()->rest('GET','/admin/shop.json');
            if(!$shopRequest['errors']){
                $shopInfo = $shopRequest['body']['shop'];
            }
            return $shopInfo;

          }catch(Exception $e){
              logger("Error ::  shopify_getShopInfo");
              logger(json_encode($e->getMessage()));
          }
     }

     public function shopify_getShopThemes($shop){
      $shopThemesInfo = [];
      try{

        $shopThemesRequest = $shop->api()->rest('GET','/admin/themes.json');

        if(!$shopThemesRequest['errors']){
            $shopThemesInfo = $shopThemesRequest['body']['themes'];
        }
        return $shopThemesInfo;

      }catch(Exception $e){
          logger("Error ::  shopify_getShopThemes");
          logger(json_encode($e->getMessage()));
      }
 }


      public function shopify_updateThemeLiquid($shop,$theme_id,$asset_key,$asset_value){
     
        try{

          $parameter['asset']['key'] = $asset_key;
          $parameter['asset']['value'] = $asset_value;

          $shopupdateThemesRequest = $shop->api()->rest('PUT','/admin/themes/'.$theme_id.'/assets.json', $parameter);
          
        //  dd($shopupdateThemesRequest);

        logger("Themes update Resposne ");
        logger(json_encode($shopupdateThemesRequest));

          if(!$shopupdateThemesRequest['errors']){
               return true;
          }else{
            return false;
          }
          

        }catch(Exception $e){
            logger("Error ::  shopify_updateThemeLiquid");
            logger(json_encode($e->getMessage()));
        }
      }
 
     

     public function shopify_GetProducts($shop,$page_filter){ 
      
         //API Version is 2022-04 (Latest)
         $numProducts = 10;
         $pageQuery = 'first: '.$numProducts;

         if(isset($page_filter['cursor']))
         {                  
            $cursor = json_encode($page_filter['cursor']);

             logger("cursor".$cursor);

            if($page_filter['hasNextPage']){
                $pageQuery = 'first: '.$numProducts.', after: '.$cursor;
            }
            if($page_filter['hasPreviousPage']){
                $pageQuery = 'last: '.$numProducts.', before: '.$cursor; 
            }
         }

         logger("product page Query :: ");
         logger($pageQuery);
                

         $resposneData = [];

         $query = 'query
                {
                    products('.$pageQuery.') {
                        pageInfo {
                            hasNextPage
                            hasPreviousPage
                            endCursor
                            startCursor
                          }
                        edges {
                            cursor
                            node{
                                id,
                                legacyResourceId,
                                productType,
                                tags,
                                title,
                                vendor,
                                handle,
                                status,
                                variants(first:1){
                                    edges{
                                      node{
                                        id  
                                        legacyResourceId
                                        title  
                                        price                                        
                                        sku
                                      }
                                    }
                                  }
                                images(first: 1) {
                                    edges {
                                        node {
                                            id
                                            originalSrc
                                            altText
                                            width
                                            height
                                            transformedSrc
                                        }
                                    }
                                }                  
                            }
                        }
                 }
               }';

         $Request =  $shop->api()->graph($query);   
         
         if(!$Request['errors']){
             $data = $Request['body']['data'];
             $products = $data['products']['edges'];
             $pageInfo = $data['products']['pageInfo'];
             $resposneData['products']  = $products;
             $resposneData['pageInfo']  = $pageInfo;

         }

         return $resposneData;
       
     }


     public function createResponseProductsFromQl($products)
     {
         try{
          $productsCollection = [];
          foreach($products as $key=>$product){

            $productItem =$product['node'];

            $item = [];

            $productIdStr = $productItem['id'];
            $productIdParts = explode('/', $productIdStr);
            $productId = end($productIdParts);

            $productImage = !empty($productItem['images']['edges'][0]) ? $productItem['images']['edges'][0]['node']['originalSrc'] : '';

            $item['id'] = $productId;
            $item['title'] = $productItem['title'];
            $item['productType'] = $productItem['productType'];       
            $item['vendor'] = $productItem['vendor'];
            $item['handle'] = $productItem['handle'];
            $item['status'] = $productItem['status'];
            $item['image'] = $productImage;

            $productsCollection[] = $item;
          }

          return $productsCollection;

         }catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
         ]);

         }         
     }

     public function shopify_GetProductByID($shop,$query_params){ 
        try{
         $product_id = $query_params['id'];
         $resposneData = [];
         $queryParam = 'id: '.json_encode($product_id);
         $graphQL = 'query
                {
                    product('.$queryParam.') {
                                id,
                                legacyResourceId
                                productType,
                                tags,
                                title,
                                vendor,
                                handle,
                                status,   
                                description,                           
                                images(first: 1) {
                                    edges {
                                        node {
                                            id
                                            originalSrc
                                            altText
                                            width
                                            height
                                            transformedSrc
                                        }
                                    }
                                } 
                                options {                                    
                                    name                                    
                                  }
                            }                
               }';

         $Request =  $shop->api()->graph($graphQL);  

         if(!$Request['errors']){
            $data = $Request['body']['data'];
            $result = [];
            $product = $data['product'];   
            $result = $product;
            $productImage = !empty($product['images']['edges'][0]) ? $product['images']['edges'][0]['node']['originalSrc'] : '';
            $result['image'] = $productImage;
            
            $resposneData = $result;            
        }

        return $resposneData;

        }catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
         ]);

         } 
     }   
     public function shopify_GetVariantByID($shop,$query_params){ 
        try{
         $variant_id = $query_params['id'];
         $resposneData = [];
         $queryParam = 'id: '.json_encode($variant_id);
         $graphQL = 'query
                {
                    productVariant('.$queryParam.') {
                                    id
                                    price
                                    sku
                                    image {
                                    originalSrc
                                    }
                                    selectedOptions {
                                        name
                                        value
                                      }
                                    inventoryQuantity
                                    title
                                    position
                                    inventoryManagement
                                    inventoryPolicy
                                    legacyResourceId                                    
                            }                
               }';

         $Request =  $shop->api()->graph($graphQL);  

         if(!$Request['errors']){
            $data = $Request['body']['data'];
            $productVariant = $data['productVariant'];          
            $resposneData = $productVariant;            
        }

        return $resposneData;

        }catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
         ]);

         } 
     }  
     
     
     public function shopify_addProductToCart($shop,$query_params){
         try{
             
         $merchandiseId = $query_params['variantId'];
         $quantity = $query_params['quantity'];
         $graphQL = 'mutation {
            cartCreate(
              input: {
                lines: [
                  {
                    quantity: '.$quantity.'
                    merchandiseId: '.json_encode($merchandiseId).'
                  }
                ]
                attributes: { key: "cart_attribute", value: "This is a cart attribute" }
              }
            ) {
              cart {
                id
                createdAt
                updatedAt
                lines(first: 10) {
                  edges {
                    node {
                      id
                      merchandise {
                        ... on ProductVariant {
                          id
                        }
                      }
                    }
                  }
                }
                attributes {
                  key
                  value
                }
                estimatedCost {
                  totalAmount {
                    amount
                    currencyCode
                  }
                  subtotalAmount {
                    amount
                    currencyCode
                  }
                  totalTaxAmount {
                    amount
                    currencyCode
                  }
                  totalDutyAmount {
                    amount
                    currencyCode
                  }
                }
              }
            }
          }';

  $Request =  $shop->api()->graph($graphQL);  

  dd($Request);

         }catch(Exception $e){
            return Response::json([
                "success" => false,
                "data" => [],
                "message" => $e->getMessage()
            ]);
         }
     }
   

}


