<?php

namespace App\Traits;
use Exception;
use Response;
use App\Models\Customizations;
use App\Models\User;
trait SettingsTrait{
         
     //=======================================================================================================
     //Settings save
     //=======================================================================================================
        
     public function SaveDefaultSettings($user_id){

        logger("START :: save Settings :: shop Id => ".$user_id);

        $settings = [
            "upsell_grid_bg_color"  => "#ffffff",
            "upsell_grid_border_color"  => "#d5d5d5",
            "upsell_grid_border_size" => "0",
            "upsell_grid_border_radius" => "10",
            "upsell_grid_padding" => "15",
            "upsell_grid_placement" => "above_add_to_cart_button",
            "upsell_grid_box_shadow" => [
                "type" => "outset",
                "x_offset" => 0,
                "y_offset" => 0,
                "blur" => 8,
                "spread" => 0,
                "color" => "#d5d5d5"
            ],
            "upsell_add_btn_text" => "Add",
            "upsell_add_btn_bg_color" => "#111111",
            "upsell_add_btn_bg_hover_color" => "#111111",
            "upsell_add_btn_border_color" => "#000000",
            "upsell_add_btn_border_radius" => "0",
            "upsell_add_btn_border_size" => "1",   
            "upsell_add_btn_text_font_size" => "14",
            "upsell_add_btn_text_font_weight" => "700",
            "upsell_add_btn_text_font_color" => "#ffffff",
            "upsell_add_btn_text_font_hover_color" => "#ffffff",
            "upsell_add_btn_text_alignment" => "center",
            "upsell_add_btn_padding" => "0",
            "upsell_add_btn_is_loader_turn_on" => true,
            "upsell_notification_is_turn_on" => "true",
            "upsell_notification_bg_color" => "#ffffff",
            "upsell_notification_border_color" => "#d5d5d5",
            "upsell_notification_border_radius" => "10",
            "upsell_notification_border_size" => "0",
            "upsell_notification_text_font_color" => "#111111",
            "upsell_notification_add_cart_text" => "Added to Cart",
            "upsell_notification_remove_cart_text" => "Removed from Cart",
            "upsell_product_text_font_size" => "16",
            "upsell_product_text_font_weight" => "600",
            "upsell_product_text_font_color" => "#000000",
            "upsell_product_text_alignment" => "left",
            "upsell_variation_btn_bg_color" => "#ffffff",
            "upsell_variation_btn_border_color"  => "#000000",
            "upsell_variation_btn_border_radius"  => "0",
            "upsell_variation_btn_border_size"   => "1",   
            "upsell_variation_btn_text_font_size" => "14",
            "upsell_variation_btn_text_font_weight" => "400",
            "upsell_variation_btn_text_font_color" => "#111111",           
            "upsell_variation_btn_padding" => "0",
            "upsell_variation_btn_bg_hover_color" => "#F3F3F3",
            "upsell_slider_settings" => [
                "slide_badge_bg_color" => "#4033df",
                "slide_badge_text_font_color" => "#ffffff",               
                "is_slider_turn_on"  => true,
                "is_slide_badge_turn_on"  => true,
                "arrows" => true,
                "arrows_color" => "#111111",
                "dots" => true,                         
                "infinite" => false,
                "speed" => 500,               
                "slidesToScroll" => 1,
                "autoplay" => false,
                "autoplaySpeed" =>3000,
                "rows" =>1,
                "dots_color" => "#4937ff",
                "active_dots_color" =>"#4937ff"
            ] 
        ];
    
        $existSetting = Customizations::where('user_id', $user_id)->first();
        $customizations = $existSetting ? $existSetting : Customizations::firstOrNew(['user_id' => $user_id]);
        $customizations->is_app_enable = true;
        $customizations->settings = $settings;
        $customizations->save();

        logger("END :: save Settings");

        return true;

    }
  

     //@end :: Settings save

     

     public function getDefaultSettings($user_id){

        $settingsInfo = [];

        $settings = Customizations::where('user_id',$user_id)->select('is_app_enable','settings')->first();  
        if($settings){
            $settingsInfo['is_app_enable'] = $settings->is_app_enable;  
            $settingsInfo['upsell_grid_placement'] = $settings->settings['upsell_grid_placement']; 
        } 

         return $settingsInfo;

     }







}

