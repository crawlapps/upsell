<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Exception;
class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            logger("========START :: PlansTableSeeder=============");

            DB::table('plans')->delete();

              if(DB::table('plans')->count()==0){

                   DB::table('plans')->insert([
                        "type" => "RECURRING",
                        "name" => "Starter",
                        "price" => 15,
                        "interval" => "EVERY_30_DAYS",
                        "capped_amount" => 0.00,
                        "terms" => "Monthly subscription",
                        "trial_days" => 14,
                        "test" => 1,
                        "on_install" => 1,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                   ]);
              }

            logger("========END :: PlansTableSeeder=============");

        }catch(Exception $e){
            logger("===============ERROR :: PlansTableSeeder ===============");
            logger(json_encode($e->getMessage()));
        }
    }
}
