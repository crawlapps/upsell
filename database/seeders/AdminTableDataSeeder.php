<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        logger("SEED::Admin Table");
        
        
         if(Admin::count() <=0){
            $user = [
                [
                    'name'=>'Admin',
                    'email'=>'admin@123.com',
                    'password'=> bcrypt('@NAuaLfYX^JB'),
                ],
                [
                    'name'=>'Admin',
                    'email'=>'admin@gmail.com',
                    'password'=> bcrypt('hA&@mL@eLR%c'),
                ],
                [
                    'name'=>'Admin',
                    'email'=>'admin@admin.com',
                    'password'=> bcrypt('$vvqrMS%FYhF'),
                ],
            ];

             foreach($user as $key=>$value){
                 Admin::create($value);
             }

         }
    }
}
