<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpSellsProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('up_sells_product_variants', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id_1')->unsigned();
            $table->string('product_id_2')->nullable();
            $table->bigInteger('shopify_id')->nullable();
            $table->string('title')->nullable();                       
            $table->string('image')->nullable(); 
            $table->decimal('price', 8, 2);
            $table->json('selectedOptions')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_id_1')->references('id')->on('up_sells_products')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('up_sells_product_variants');
    }
}
