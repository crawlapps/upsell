<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
            $table->string('shopify_customer_id')->nullable();
            $table->string('email')->nullable();
            $table->boolean('accepts_marketing')->default(false);
            $table->boolean('verified_email')->default(false);
            $table->string('currency')->nullable();
            $table->string('admin_graphql_api_id')->nullable();
            $table->json('default_address')->nullable();
            $table->bigInteger('upsell_orders_count')->default(0);	
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
