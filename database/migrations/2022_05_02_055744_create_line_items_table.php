<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('db_order_id');
            $table->foreign('db_order_id')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->string('shopify_lineitem_id')->nullable();
            $table->string('name')->nullable();
            $table->decimal('price', 8, 2);
            $table->bigInteger('product_id');
            $table->bigInteger('variant_id');
            $table->decimal('quantity')->nullable();
            $table->json('full_response')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_items');
    }
}
