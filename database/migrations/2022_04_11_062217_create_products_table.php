<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();       
            $table->bigInteger('offer_id')->unsigned();
            $table->bigInteger('shopify_id')->unsigned()->nullable();
            $table->string('gid')->nullable();
            $table->string('title')->nullable();
            $table->string('handle')->nullable();           
            $table->string('image')->nullable();           
            $table->timestamps();
            $table->softDeletes();            
            $table->foreign('offer_id')->references('id')->on('up_sells_offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
