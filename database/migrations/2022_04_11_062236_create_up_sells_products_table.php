<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpSellsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('up_sells_products', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('product_db_id')->unsigned();
            $table->string('product_id_1')->nullable();
            $table->bigInteger('shopify_id')->nullable();
            $table->string('handle')->nullable(); 
            $table->string('title')->nullable();                       
            $table->string('image')->nullable();  
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_db_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('up_sells_products');
    }
}
