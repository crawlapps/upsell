<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TestController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CustomizationsController;
use App\Http\Controllers\AnalyticsController;
use App\Http\Controllers\ManageUpsellProductsController;
use App\Http\Controllers\HelpController;
use App\Http\Controllers\InstallationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::get('/', function () {
    return view('pages.home');
})->middleware(['verify.shopify','billable'])->name('home');;



Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/login', function () {
    return view('auth.login');
})->name('login');


Route::group(['middleware' => ['verify.shopify','billable']], function () {

    Route::get('/test', [TestController::class, 'index']);
    Route::get('/test/sproduct',[TestController::class,'getShopifyProducts']);
    Route::get('/test/orderjob',[TestController::class,'orderjob']);
    Route::get('/test/productjob',[TestController::class,'productUpdatejob']);

    Route::POST('/shopify/products',[ManageUpsellProductsController::class,'getShopifyProducts'])->name('shopify.products');
    Route::get('/upsell/products/{shopify_id}',[ManageUpsellProductsController::class,'getUpSellsProducts'])->name('upsell.products');
    Route::POST('/upsell/products/attach',[ManageUpsellProductsController::class,'attachUpSellsProducts'])->name('upsell.attach.products');

    // Offer
    Route::POST('/offer/get',[ManageUpsellProductsController::class,'getOffer'])->name('offer.get');
    Route::POST('/offer/add',[ManageUpsellProductsController::class,'addOffer'])->name('offer.add');
    Route::POST('/offer/add/check-name-exist',[ManageUpsellProductsController::class,'addOfferNameCheck'])->name('offer.add.check-name-exist');
    Route::get('/offer/edit/{id}',[ManageUpsellProductsController::class,'editOffer'])->name('offer.edit');
    Route::POST('/offer/update/{id}',[ManageUpsellProductsController::class,'updateOffer'])->name('offer.update');
    Route::POST('/offer/delete',[ManageUpsellProductsController::class,'deleteOffers'])->name('offer.delete');
    Route::POST('/offer/change-status',[ManageUpsellProductsController::class,'changeStatusForOffers'])->name('offer.change-status');
    //@end:: Offer

    Route::get('/settings',[CustomizationsController::class,'getSettings'])->name('settings');
    Route::POST('/settings',[CustomizationsController::class,'saveSettings'])->name('settings.save');
    Route::get('/settings/get-app-status',[CustomizationsController::class,'getStatusForApp'])->name('settings.get.status');
    Route::POST('/settings/change-app-status',[CustomizationsController::class,'changeStatusForApp'])->name('settings.update.status');
    Route::get('/settings/reset',[CustomizationsController::class,'resetSettings'])->name('settings.reset');

    Route::get("/user/shop-info",[DashboardController::class,'getShopInfo'])->name('shop-info');
    Route::POST("/dashboard/analytics/total-info",[DashboardController::class,'getTotalAnalyticsInfo'])->name('dashboard.analytics.total-info');
   // Route::POST("dashboard/analytics/revenue-graph-info",[DashboardController::class,'getRevenueGraphInfo'])->name('dashboard.analytics.revenue-graph-info');

    Route::POST("/analytics/total-info",[AnalyticsController::class,'getTotalAnalyticsInfo'])->name('analytics.total-info');
    Route::POST("/analytics/revenue-graph-info",[AnalyticsController::class,'getRevenueGraphInfo'])->name('analytics.revenue-graph-info');
    Route::POST("/analytics/top-added-cart-poducts",[AnalyticsController::class,'getTopAddedCartPoductsInfo'])->name('analytics.top-added-cart-poducts');

    Route::get("/shop/themes",[InstallationController::class,'getShopThemes'])->name('shop.themes');
    Route::POST("/shop/themes/install",[InstallationController::class,'installShopThemes'])->name('shop.themes.install');
    Route::POST("/shop/themes/uninstall",[InstallationController::class,'unInstallShopThemesSnippets'])->name('shop.themes.uninstall');
        

});


// TEST MODE  ----------------------------------------------------------

Route::get('/test/shop',[TestController::class,'shopData']);

// END :: TEST MODE -----------------------------------------------------





