<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\frontend\ManageUpsellProductsController;
use App\Http\Controllers\frontend\CustomizationsController;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, post, get');
header("Access-Control-Max-Age", "3600");
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
header("Access-Control-Allow-Credentials", "true");

// 'front' // is prefix


Route::POST('/upsell/setting',[ManageUpsellProductsController::class,'getUpSellsSetting'])->name('front.upsell.setting');

Route::POST('/upsell/products',[ManageUpsellProductsController::class,'getUpSellsProducts'])->name('front.upsell.products');
Route::POST('/upsell/products/cart/add',[ManageUpsellProductsController::class,'AddUpSellsProductsToCart'])->name('front.upsell.products.add');
Route::POST('/upsell/analytics/click/add',[ManageUpsellProductsController::class,'AddOfferClick'])->name('front.offer.click.add');
//Route::Post('/upsell/settings',[CustomizationsController::class,'getSettings'])->name('front.upsell.settings');