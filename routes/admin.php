<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\AdminAuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\MerchantsController;

Route::get('/', [AdminAuthController::class, 'getLogin'])->name('adminLogin');
Route::get('/login', [AdminAuthController::class, 'getLogin'])->name('adminLogin');

Route::POST('/login', [AdminAuthController::class, 'postLogin'])->name('adminLoginPost');
Route::POST('/logout', [AdminAuthController::class, 'logout'])->name('adminLogout');


Route::group(['middleware' => 'adminauth'], function () {
	// Admin Dashboard
	Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
	Route::get('/change-password', [DashboardController::class, 'changePassword'])->name('admin.change-password');
	Route::post('/update-password', [DashboardController::class, 'updatePassword'])->name('admin.update-password');
	Route::get('/merchants', [MerchantsController::class, 'index'])->name('admin.merchants.index');
	Route::get('/merchants/list', [MerchantsController::class, 'getMerchants'])->name('admin.getMerchants');	
});


?>