<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>        
        @include('includes.head')
        @include('includes.favicon')

        @if(config('shopify-app.appbridge_enabled'))
        @include('shopify.app-bridge')
        @endif

        <?php
        header("Content-Security-Policy: frame-ancestors https://".auth()->user()->name."  https://admin.shopify.com");
        ?>

    </head>
    <body class="antialiased">
       
        
        <div id="app">
            @yield('content')
        </div>
        
        <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
        @include('includes.footer-scripts')
    </body>
</html>
