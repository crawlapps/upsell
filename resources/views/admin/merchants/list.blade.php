@extends('admin.layouts.default')
@section('content')
  
<h1 class="app-page-title">Merchants</h1>			    

<div class="app-card app-card-orders-table shadow-sm mb-5 p-3">
    <div class="app-card-body">
        <div class="table-responsive">
            <table id='merchantsTable' style='border-collapse: collapse;'   width="100%" class="table app-table-hover mb-0 text-left">
{{-- <table id='merchantsTable' width='100%' border="1" style='border-collapse: collapse;' class="table table-bordered"  width="100%" cellspacing="0"> --}}
    <thead>
        <tr>
             <td class="cell">ID</td>             
             <td class="cell">Store URL</td>
             <td class="cell">Email</td>
             <td>Installed Date</td>
        </tr>
     </thead>
 </table>
</div>
</div>
</div>


 <!-- Script -->
 <script type="text/javascript"> 
 $(document).ready(function(){

    console.log("get users::admin");

     // DataTable
    $('#merchantsTable').DataTable({
         processing: true,
         serverSide: true,
         ajax: "{{route('admin.getMerchants')}}",
         columns: [
            {
                      data: 'id',
                     "searchable": true,
                      bSortable: true,
                      mRender: function(data, type, row) {                                               
                      return row.id;
                      }
            },
            {
                      data: 'name',
                     "searchable": true,
                      bSortable: true,
                      mRender: function(data, type, row) {                                               
                          return row.name;
                      }
            },
            {
                      data: 'email',
                     "searchable": true,
                      bSortable: true,
                      mRender: function(data, type, row) {                                               
                          return row.email;
                      }
            },
            {
                      data: 'created_at',
                     "searchable": true,
                      bSortable: true,
                      mRender: function(data, type, row) {                                               
                          return row.created_at;
                      }
            }
         ]
     });

  });
  </script>


@endsection