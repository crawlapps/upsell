
    <!-- Javascript -->          
    <script src="{{asset('admin-theme-bs5/assets/plugins/popper.min.js')}}"></script>
    <script src="{{asset('admin-theme-bs5/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>  

    <!-- Charts JS -->
    <script src="{{asset('admin-theme-bs5/assets/plugins/chart.js/chart.min.js')}}"></script> 
    <script src="{{asset('admin-theme-bs5/assets/js/index-charts.js')}}"></script> 
    
    <!-- Page Specific JS -->
    <script src="{{asset('admin-theme-bs5/assets/js/app.js')}}"></script> 


    <!-- jQuery Library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- Datatable JS -->
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>


