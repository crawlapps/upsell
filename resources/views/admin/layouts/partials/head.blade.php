<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
<meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
<link rel="shortcut icon" href="favicon.ico"> 

<script src="{{asset('js/jquery-3.6.0.min.js')}}"></script>
<!-- FontAwesome JS-->
<script defer src="{{asset('admin-theme-bs5/assets/plugins/fontawesome/js/all.min.js')}}"></script>

<!-- App CSS -->  
<link id="theme-style" rel="stylesheet" href="{{asset('admin-theme-bs5/assets/css/portal.css')}}">


<!-- Datatable CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css"/>

