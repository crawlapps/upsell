<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>{{ config('app.name', 'Product Page Upsell') }}</title>
    
     @include('admin.layouts.partials.head')
     @include('includes.favicon')
   
</head> 

<body class="app">   	
 
   @include('admin.layouts.partials.header')
    
    <div class="app-wrapper">
	    
	    <div class="app-content pt-3 p-md-3 p-lg-4">
		    <div class="container-xl">

                @yield('content')	    			
			    
		    </div><!--//container-fluid-->
	    </div><!--//app-content-->
	    	 
        
        @include('admin.layouts.partials.footer')
	    
    </div><!--//app-wrapper-->  
    
    
    @include('admin.layouts.partials.footer-scripts')
 

</body>
</html> 

