<script src="https://unpkg.com/@shopify/app-bridge"></script>
<script>
    var AppBridge = window['app-bridge'];
    var createApp = AppBridge.default;

    window.shopify_app_bridge = createApp({
        apiKey: '{{ config('shopify-app.api_key') }}',
        shopOrigin: '{{ Auth::user()->name }}',
        forceRedirect: true,
        host : '{{ Auth::user()->name }}'
    });
</script>