import FrontendApp from "./Frontend/CustomProducts.vue";
import axios from 'axios';

if (window.UPSELL_LOADED != true) {

    window.UPSELL_LOADED = true;

    console.log("UPSELL::VUE SCRIPT JS");

    const host = process.env.MIX_APP_URL;

    console.log("host", host);

    // START :: JS ======================================================================

    var head = document.getElementsByTagName("head")[0];
    var script = document.createElement('script');
    //  console.log("script", script);
    script.onload = function() {
        console.log("------------------------------- Script Loded ---------------------------");

        $(document).ready(function() {

            let url = window.location.href.split('?')[0];

            if (url.includes("/")) {
                let handle = url.split('/').pop();
                if (url.includes("/products/") && handle) {

                    var shopifyDomainShop = getParams('upsell_shopify');
                    var shopifyDomain = shopifyDomainShop.shop;

                    let endPoint = host + "/front/upsell/setting";

                    let params = {
                        "shopify_domain": shopifyDomain,
                    };

                    axios.post(endPoint, params).then((res) => {
                        if (res.data.success) {

                            if (res.data.settings.is_app_enable || res.data.settings.is_app_enable == 1 || res.data.settings.is_app_enable == '1') {
                                console.log("is_app_enable Turn On");

                                if (res.data.settings.upsell_grid_placement) {
                                    var upsell_grid_placement = res.data.settings.upsell_grid_placement;

                                    console.log("upsell_grid_placement", upsell_grid_placement);

                                    findProductForm(upsell_grid_placement);

                                } else {
                                    makeUpsellClassSnippet();
                                }
                            } else {
                                console.log("is_app_enable Turn Off");
                            }
                        }
                    }).catch(function(error) {
                        console.log("error :: ", error);
                    }).finally(res => {

                    });

                } else {
                    console.log("Current page is not product page");
                }
            }

            function findProductFormButton() {

                var btn = document.querySelector(
                    'form[action*="/cart/add"] button[name*="add"]'
                );

                if (!btn) {

                    btn = document.querySelector(
                        'form[action*="/cart/add"] button[name*="submit"]'
                    );
                }

                if (!btn) {

                    btn = document.querySelector(
                        'form[action*="/cart/add"] input[name*="add"]'
                    );
                }


                return btn;

            }

            function findProductForm(upsell_grid_placement) {

                let upsellClassSnippet = document.getElementsByClassName('upsell-offers');

                //  $(".upsell-offers").html("<p>upsell</p>");



                if (upsellClassSnippet.length > 0) {

                    console.log("upsell-offers class found");

                    window.Vue = require('vue').default;

                    const app = new window.Vue({
                        el: '.upsell-offers',
                        render: h => h(FrontendApp)
                    });

                } else {

                    console.log("upsell-offers class not found");

                    var productFormbtn = findProductFormButton();

                    console.log("productFormbtn", productFormbtn);

                    if (productFormbtn) {
                        // elements with class "snake--mobile" exist

                        if (upsell_grid_placement == "above_add_to_cart_button") {

                            let upsellapp_above_div = document.createElement('div');
                            upsellapp_above_div.setAttribute('id', 'upsellapp_above');
                            productFormbtn.before(upsellapp_above_div);

                            window.Vue = require('vue').default;
                            const app = new window.Vue({
                                el: '#upsellapp_above',
                                render: h => h(FrontendApp)
                            });

                        }

                        if (upsell_grid_placement == "below_add_to_cart_button") {

                            let upsellapp_below_div = document.createElement('div');
                            upsellapp_below_div.setAttribute('id', 'upsellapp_below');
                            productFormbtn.after(upsellapp_below_div);

                            window.Vue = require('vue').default;
                            const app1 = new window.Vue({
                                el: '#upsellapp_below',
                                render: h => h(FrontendApp)
                            });
                        }
                    }
                }
                //  else {
                //     console.log("not class found.");
                //     makeUpsellClassSnippet();

                // }
            }

            //=======================================================================================================================================
            // Snippet : <div class="upsell-offers"></div>
            //=======================================================================================================================================
            function makeUpsellClassSnippet() {

                console.log("Snippet : <div class='upsell-offers'></div> NOW Woring.");

                //    <div class="upsell-offers"></div>

                let upsellClassSnippet = document.getElementsByClassName('upsell-offers');

                //  $(".upsell-offers").html("<p>upsell</p>");

                if (upsellClassSnippet.length > 0) {
                    window.Vue = require('vue').default;
                    const app = new window.Vue({
                        el: '.upsell-offers',
                        render: h => h(FrontendApp)
                    });

                }

            }
            //=======================================================================================================================================
            //@end
            //=======================================================================================================================================


            function getParams(script_name) {
                // Find all script tags
                var scripts = document.getElementsByTagName("script");
                // Look through them trying to find ourselves
                for (var i = 0; i < scripts.length; i++) {
                    if (scripts[i].src.indexOf("/" + script_name) > -1) {
                        // Get an array of key=value strings of params
                        var pa = scripts[i].src.split("?").pop().split("&");
                        // Split each key=value into array, the construct js object
                        var p = {};
                        for (var j = 0; j < pa.length; j++) {
                            var kv = pa[j].split("=");
                            p[kv[0]] = kv[1];
                        }
                        return p;
                    }
                }
                // No scripts match

                return {};
            }


        });
    };
    script.type = 'text/javascript';
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js';

    head.appendChild(script);

}



// END :: JS ======================================================================