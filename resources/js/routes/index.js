import VueRouter from 'vue-router';
window.Vue = require('vue').default;
Vue.use(VueRouter);

// import Home from '../components/HomeComponent.vue'; // test 
// import About from '../components/AboutComponent.vue'; //test 

import Dashboard from '../components/pages/Dashboard';
import ManageUpsellProducts from '../components/pages/ManageUpsellProducts';
import Analytics from '../components/pages/Analytics';
import Customizations from '../components/pages/Customizations';
import Help from '../components/pages/Help';
import AddUpsell from '../components/pages/upsell/AddUpsell';
import EditUpsell from '../components/pages/upsell/EditUpsell';
import SnippetsUninstall from '../components/pages/SnippetsUninstall';


const routes = [
    // {
    //     path: '/',
    //     component: AddUpsell,
    //     name: 'addupsellpage1',
    //     title: 'addupsellpage1',
    //     meta: {
    //         ignoreInMenu: 0
    //     },
    // },
    {
        path: '/',
        component: Dashboard,
        name: 'dashboard',
        title: 'Dashboard',
        meta: {
            ignoreInMenu: 0
        },
    },
    {
        path: '/manage-upsell-products',
        component: ManageUpsellProducts,
        name: 'manage-upsell-products',
        title: 'Upsell',
        meta: {
            ignoreInMenu: 0
        },
    },
    {
        path: '/analytics',
        component: Analytics,
        name: 'analytics',
        title: 'Analytics',
        meta: {
            ignoreInMenu: 0
        },
    },
    {
        path: '/customizations',
        component: Customizations,
        name: 'customizations',
        title: 'Settings',
        meta: {
            ignoreInMenu: 0
        },
    },
    {
        path: '/addupsellpage',
        component: AddUpsell,
        name: 'addupsellpage',
        title: 'Add Upsell',
        meta: {
            ignoreInMenu: 1
        },
    },
    {
        path: '/editupsellpage/:id',
        component: EditUpsell,
        name: 'editupsellpage',
        title: 'Edit Upsell',
        meta: {
            ignoreInMenu: 1
        },
    },
    {
        path: '/help',
        component: Help,
        name: 'help',
        title: 'Help',
        meta: {
            ignoreInMenu: 1
        },
    },
    {
        path: '/snippets-uninstall',
        component: SnippetsUninstall,
        name: 'snippets-uninstall',
        title: 'Snippets Uninstall',
        meta: {
            ignoreInMenu: 1
        },
    },
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;