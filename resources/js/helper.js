import { Loading, Toast, Modal, Button, contextualSaveBar, ResourcePicker } from '@shopify/app-bridge/actions';
var loading = '';
export default {

    initLoading() {
        loading = Loading.create(window.shopify_app_bridge);
    },
    startLoading() {
        this.initLoading();
        loading.dispatch(Loading.Action.START);
    },
    stopLoading() {
        this.initLoading();
        loading.dispatch(Loading.Action.STOP);
    },
    successToast(message) {
        let toastNotice = Toast.create(window.shopify_app_bridge, { message: message, duration: 3000 });
        toastNotice.dispatch(Toast.Action.SHOW);
    },
    errorToast(message) {
        let toastNotice = Toast.create(window.shopify_app_bridge, { message: message, duration: 3000, isError: true });
        toastNotice.dispatch(Toast.Action.SHOW);
    },
    confirmModel() {

        const okButton = Button.create(window.shopify_app_bridge, { label: 'Ok' });
        okButton.subscribe(Button.Action.CLICK, () => {
            console.log('Ok okButton');
        });

        const cancelButton = Button.create(window.shopify_app_bridge, { label: 'Cancel' });
        cancelButton.subscribe(Button.Action.CLICK, () => {
            console.log('Ok cancelButton');
            myModal.dispatch(Modal.Action.CLOSE);
        });
        const modalOptions = {
            title: 'My Modal',
            message: "My message",
            footer: {
                buttons: {
                    primary: okButton,
                    secondary: [cancelButton],
                },
            },
        };

        const myModal = Modal.create(window.shopify_app_bridge, modalOptions);
        myModal.dispatch(Modal.Action.OPEN);

        // myModal.subscribe(Modal.Action.OPEN, () => {
        //     console.log('Ok OPEN');
        // });
        //
        // myModal.subscribe(Modal.Action.CLOSE, () => {
        //     console.log('Ok CLOSE');
        //     myModal.dispatch(Modal.Action.CLOSE);
        // });
    },


}