/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import App from './components/layouts/App.vue';
import router from './routes/index.js';


import PolarisVue from '@hulkapps/polaris-vue';
import '@hulkapps/polaris-vue/dist/polaris-vue.css';
Vue.use(PolarisVue);

import "font-awesome/css/font-awesome.min.css";
Vue.component('VueFontawesome', require('vue-fontawesome-icon/VueFontawesome.vue').default);
Vue.component('colorpicker', require('./components/partials/ColorPicker.vue').default);

import DateRangePicker from 'vue2-daterange-picker';
import 'vue2-daterange-picker/dist/vue2-daterange-picker.css';







// import { Chrome } from "vue-color";
// Vue.component('colorpicker1', {
//     components: {
//         'chrome-picker': Chrome,
//     },
//     template: `
// <div class="input-group color-picker" ref="colorpicker">
//     <input type="text" class="Polaris-TextField__Input" v-model="colorValue" @focus="showPicker()" @input="updateFromInput" />
//     <div class="Polaris-TextField__Spinner" aria-hidden="true">
//     <div role="button" class="Polaris-TextField__Segment current-color"  :style="'background-color: ' + colorValue" @click="togglePicker()">
// 	<span  class="input-group-addon color-picker-container">
// 			<chrome-picker :value="colors" @input="updateFromPicker" v-if="displayPicker" />
//     </span>
//     </div>
//     </div>
// </div>`,
//     props: ['color'],
//     data() {
//         return {
//             colors: {
//                 hex: '#000000',
//             },
//             colorValue: '',
//             displayPicker: false,
//         }
//     },
//     mounted() {
//         //console.log("mounted color :: ", this.color);
//         this.setColor(this.color || '#000000');
//     },
//     created() {
//         // console.log("created color :: ", this.color);
//         // this.setColor(this.color || '#000000');
//     },
//     updated() {
//         // console.log("updated color :: ", this.color);
//         //this.setColor(this.color || '#000000');
//     },
//     methods: {
//         setColor(color) {
//             this.updateColors(color);
//             this.colorValue = color;
//         },
//         updateColors(color) {
//             if (color.slice(0, 1) == '#') {
//                 this.colors = {
//                     hex: color
//                 };
//             } else if (color.slice(0, 4) == 'rgba') {
//                 var rgba = color.replace(/^rgba?\(|\s+|\)$/g, '').split(','),
//                     hex = '#' + ((1 << 24) + (parseInt(rgba[0]) << 16) + (parseInt(rgba[1]) << 8) + parseInt(rgba[2])).toString(16).slice(1);
//                 this.colors = {
//                     hex: hex,
//                     a: rgba[3],
//                 }
//             }
//         },
//         showPicker() {
//             document.addEventListener('click', this.documentClick);
//             this.displayPicker = true;
//         },
//         hidePicker() {
//             document.removeEventListener('click', this.documentClick);
//             this.displayPicker = false;
//         },
//         togglePicker() {
//             this.displayPicker ? this.hidePicker() : this.showPicker();
//         },
//         updateFromInput() {
//             this.updateColors(this.colorValue);
//         },
//         updateFromPicker(color) {
//             this.colors = color;
//             if (color.rgba.a == 1) {
//                 this.colorValue = color.hex;
//             } else {
//                 this.colorValue = 'rgba(' + color.rgba.r + ', ' + color.rgba.g + ', ' + color.rgba.b + ', ' + color.rgba.a + ')';
//             }
//         },
//         documentClick(e) {
//             var el = this.$refs.colorpicker,
//                 target = e.target;
//             if (el !== target && !el.contains(target)) {
//                 this.hidePicker()
//             }
//         }
//     },
//     watch: {
//         colorValue(val) {
//             if (val) {
//                 this.updateColors(val);
//                 this.$emit('input', val);
//                 //document.body.style.background = val;
//             }
//         },
//     },
// });

const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router
});